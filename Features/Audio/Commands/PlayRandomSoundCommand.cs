//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using System.Collections.Generic;

namespace Nonatomic.Utomic {

	public class PlayRandomSoundCommand : ICommand {

		public PlayRandomSoundCommand(){

		}
		
		public void Execute(params object[] parameters) {

			var sounds = (List<string>) parameters[0];
			int choice = new System.Random().Next(sounds.Count);
			this.Inject<IAudioManager>().PlaySound(sounds[choice], 1.0f, 1.0f);
		}
	}
}
