//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace Nonatomic.Utomic {

	public class AudioManager : IAudioManager {

		private bool mute;
		private List<AudioSource>sources = new List<AudioSource>();
		private int maxSources = 10;

		public AudioManager(){

			var game = this.EntityList().GetComponent<UtomicCore>();
			game.gameObject.AddEntityComponent<AudioListener>();
			this.EntityList().AddEntity(game.gameObject);

			IsMute();
		}

		private bool IsMute(){

			var audioVal = PlayerPrefs.GetInt("AudioToggle");

			switch(audioVal){
				case 0:
				case 2:
					mute = false;
				break;
				case 1:
					mute = true;
				break;
			}

			return mute;
		}

		private AudioSource GetFreeSource(){
			var free = sources.FindAll(x => !x.isPlaying);

			if(free.Count > 0){
				var source = free[0];
				sources.Remove(free[0]);
				sources.Add(source);
				return source;
			}
			else if(sources.Count < maxSources){
				var game = this.EntityList().GetComponent<UtomicCore>();
				var source = game.gameObject.AddEntityComponent<AudioSource>();
				sources.Add(source);
				return source;
			}
			else{
				var source = sources[0];
				sources.Remove(sources[0]);
				sources.Add(source);
				return source;
			}
		}

		public void PlaySound(string sound, float volume, float pitch){

			AudioSource audio = GetFreeSource();
			AudioClip clip = (AudioClip)Resources.Load(sound);

			if(clip != null) {
				audio.volume = volume;
				audio.clip = clip;
				audio.Play();
				audio.pitch = pitch;
			}
			else{
				this.LogError("Missing Sound:" + sound);
			}
		}
	}
}
