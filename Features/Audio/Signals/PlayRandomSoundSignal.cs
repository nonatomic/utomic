
using System.Collections.Generic;

namespace Nonatomic.Utomic {

	public class PlayRandomSoundSignal : Signal {

		public PlayRandomSoundSignal() {

			/**
			* @string sound effect name
			*/
			this.Restrict<List<string>>();
		}
	}
}
