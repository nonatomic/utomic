
namespace Nonatomic.Utomic {

	public class PlaySoundSignal : Signal {

		public PlaySoundSignal() {

			/**
			* @string sound effect name
			*/
			this.Restrict<string, float, float>();
		}
	}
}
