using Nonatomic.Utomic;

namespace Nonatomic.Utomic {

	internal class PreloadAssetsProgressCommand : ICommand {

		public void Execute(params object[] parameters){

			// string sceneName = (string) parameters[0];
			float percLoaded = (float) parameters[1];

			this.Log("Utomic Loading Progress:", percLoaded.ToString(), "orange", "yellow");

			if(this.HasBinding<UtomicLoadingProgressSignal>()){
				this.Inject<UtomicLoadingProgressSignal>().Dispatch(percLoaded);
			}
			else{
				this.LogWarning("No Command bound to UtomicLoadingProgressSignal");
			}
		}
	}
}
