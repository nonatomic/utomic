using Nonatomic.Utomic;
using UnityEngine;
using System;
using System.Collections;

namespace Nonatomic.Utomic {

	internal class PreloadAssetsCompleteCommand : ICommand {

		public void Execute(params object[] parameters){

			// string sceneName = (string) parameters[0];
			float percLoaded = (float) parameters[1];

			this.Log("Utomic Loading Complete:", percLoaded.ToString(), "orange", "green");
			this.Inject<UtomicLoadingCompleteSignal>().Dispatch(percLoaded);

			this.Log("Run Features", Color.cyan);
			this.RunFeatures();

			this.Log("Feature complete", Color.green);

			if(this.HasBinding<UtomicFeatureCompleteSignal>()){
				this.Inject<UtomicFeatureCompleteSignal>().Dispatch();
			}
			else{
				this.LogWarning("No Command bound to UtomicFeatureCompleteSignal");
			}

		}
	}
}
