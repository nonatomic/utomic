
using UnityEngine;
using System.Collections;

namespace Nonatomic.Game {

	/**
	* Use this script in the Assets scene to attach fonts to.
	* This provides a reference in the scene to the font allowing the font to
	* be preloaded along with the scene
	*/
	public class FontReference : MonoBehaviour {

		public Font[] fonts;
	}
}
