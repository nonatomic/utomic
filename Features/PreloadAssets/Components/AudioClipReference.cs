using UnityEngine;
using System.Collections;

namespace Nonatomic.Game {

	/**
	* Use this script in the Assets scene to attach audio clips to.
	* This provides a reference in the scene to the clip allowing the audio to
	* be preloaded along with the scene
	*/
	public class AudioClipReference : MonoBehaviour {

		public AudioClip[] audioClips;
	}
}
