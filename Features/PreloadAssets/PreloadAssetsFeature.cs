//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;

namespace Nonatomic.Utomic {

	internal class PreloadAssetsFeature : IFeature {

		public void Wire() {

			//internal commands
			this.CommandMap().Bind<PreloadAssetsCompleteSignal, PreloadAssetsCompleteCommand>();
			this.CommandMap().Bind<PreloadAssetsProgressSignal, PreloadAssetsProgressCommand>();

			//external signals
			this.Bind<UtomicLoadingCompleteSignal>();
			this.Bind<UtomicLoadingProgressSignal>();
		}

		public void Structure(){
			//nothing
		}

		public void Setup(){
			//do nothing
		}

		public void Run(){
			//do nothing
		}
	}
}
