
namespace Nonatomic.Utomic {

	internal class PreloadAssetsProgressSignal : Signal {

		public PreloadAssetsProgressSignal(){

			/**
			* string scene name
			* float progress
			*/
			 base.Restrict<string,float>();
		}
	}
}
