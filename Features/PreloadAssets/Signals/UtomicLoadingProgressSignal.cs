
namespace Nonatomic.Utomic {

	public class UtomicLoadingProgressSignal : Signal {

		public UtomicLoadingProgressSignal(){

			/**
			* float progress
			*/
			 base.Restrict<float>();
		}
	}
}
