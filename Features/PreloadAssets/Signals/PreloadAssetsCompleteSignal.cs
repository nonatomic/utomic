
namespace Nonatomic.Utomic {

	internal class PreloadAssetsCompleteSignal : Signal {

		public PreloadAssetsCompleteSignal(){

			/**
			* string scene name
			* float progress
			*/
			 base.Restrict<string,float>();
		}
	}
}
