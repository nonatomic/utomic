
namespace Nonatomic.Utomic {

	public class UtomicLoadingCompleteSignal : Signal {

		public UtomicLoadingCompleteSignal(){

			/**
			* float progress
			*/
			 base.Restrict<float>();
		}
	}
}
