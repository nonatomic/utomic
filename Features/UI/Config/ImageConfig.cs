//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

namespace Nonatomic.Utomic {

	public class ImageConfig {

		//image
		public string spriteName;

		//size
		public bool nativeSize = true;
		public bool preserveAspect = true;

		//style
		public Color color = Color.white;
		public Image.Type imageType = Image.Type.Simple;

		//fill style
		public float fillAmount = 0.5f;
		public Image.FillMethod fillMethod;//Vertical, Horizontal, Radial180, Radial360
		public int fillOrigin = 0;//Vertical, Horizontal, Radial180, Radial360
		public bool fillClockwise;

		//outline
		public bool outline = false;
		public Color outlineColor;
		public Vector2 outlineDistance;
		public bool outlineUseAlpha = true;
	}
}
