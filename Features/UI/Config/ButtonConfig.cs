//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

namespace Nonatomic.Utomic {

	public class ButtonConfig {

		public Signal click;
		public Signal up;
		public Signal down;
		public Signal enter;
		public Signal exit;

		public Sprite clickSprite;
		public Sprite upSprite;
		public Sprite downSprite;
		public Sprite enterSprite;
		public Sprite exitSprite;

		public float clickLockDuration = 0.1f; //spam lock buttons
		public float downLockDuration = 0.1f; //spam lock buttons
	}
}
