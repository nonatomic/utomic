//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

namespace Nonatomic.Utomic {

	public class TextStyle {

		public string text = "";

		//font
		public string fontName = "";
		public string fontMaterial = "";

		//style
		public FontStyle fontStyle = FontStyle.Normal;
		public Color color = new Color(0,0,0);

		//font size
		public int fontSize = 12;
		public int minSize = 10;
		public int maxSize = 40;
		public bool bestFit = false;

		//alignment
		public TextAnchor alignment = TextAnchor.MiddleCenter;
		public VerticalWrapMode verticalOverflow = VerticalWrapMode.Truncate;
		public HorizontalWrapMode horizontalOverflow = HorizontalWrapMode.Wrap;

		//localisation
		public string key;
	}
}
