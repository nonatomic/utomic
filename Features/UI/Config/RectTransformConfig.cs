//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

namespace Nonatomic.Utomic {

	public class RectTransformConfig {

		public Vector3 anchoredPosition = Vector3.zero;
		public Vector2 anchorMin = new Vector2(0.5f, 0.5f);
		public Vector2 anchorMax = new Vector2(0.5f, 0.5f);
		public Vector2 offsetMin = Vector2.zero;
		public Vector2 offsetMax = Vector2.zero;
		public Vector2 pivot = new Vector2(0.5f, 0.5f);
		public Vector2 sizeDelta = Vector2.zero;
		public Vector3 scale = Vector3.one;
		public Quaternion rotation = Quaternion.Euler(0, 0, 0);
	}
}
