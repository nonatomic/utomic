//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

namespace Nonatomic.Utomic {

	public class UIFactory : IUIFactory {

		public GameObject MakeContainer(string name, GameObject parent) {

			var container = new GameObject(name);
			container.transform.SetParent(parent.transform, false);

			var transform = container.AddEntityComponent<RectTransform>();
			transform.SetAnchorMinMax(Vector2.zero, Vector2.one).SetOffsetMinMax(Vector2.zero, Vector2.zero);

			this.EntityList().AddEntity(container);
			return container;
		}

		public T MakeContainer<T>(string name, GameObject parent) where T : Component {
			var component = MakeContainer(name, parent).AddEntityComponent<T>();
			return component;
		}

		public Image MakeImage(string imagePath, GameObject parent) {

			var container = MakeContainer(imagePath, parent);
			var img = container.AddEntityComponent<Image>();

			if(!string.IsNullOrEmpty(imagePath)){
				var atlasLoader = this.Inject<IAtlasLoader>();
				img.sprite = atlasLoader.GetSprite(imagePath) ?? Resources.Load<Sprite>(imagePath);
			}

			img.SetNativeSize();
			img.preserveAspect = true;

			this.EntityList().AddEntity(container);
			return img;
		}

		public T MakeImage<T>(string imagePath, GameObject parent) where T : Component {
			var component = MakeImage(imagePath, parent).gameObject.AddEntityComponent<T>();
			return component;
		}

		public GameObject MakeButton(string imagePath, GameObject parent, ButtonConfig buttonConfig) {

			GameObject go = MakeImage(imagePath, parent).gameObject;
			go.AddEntityComponent<Button>();
			go.AddEntityComponent<ButtonMonitor>().SetConfig(buttonConfig);
			this.EntityList().AddEntity(go);

			//new to unity is the Raycast target. This much be set to true for buttons to work
			go.GetComponent<Image>().raycastTarget = true;

			return go;
		}

		public GameObject MakeButton(string imagePath, GameObject parent, Signal click) {

			GameObject go = MakeImage(imagePath, parent).gameObject;
			go.AddEntityComponent<Button>();
			go.AddEntityComponent<ButtonMonitor>().SetConfig(new ButtonConfig(){click = click});
			this.EntityList().AddEntity(go);

			//new to unity is the Raycast target. This much be set to true for buttons to work
			go.GetComponent<Image>().raycastTarget = true;

			return go;
		}

		public Image MakeButton(Image img, Signal click) {
			img.gameObject.AddEntityComponent<Button>();
			img.gameObject.AddEntityComponent<ButtonMonitor>().SetConfig(new ButtonConfig(){click = click});
			this.EntityList().AddEntity(img.gameObject);

			//new to unity is the Raycast target. This much be set to true for buttons to work
			img.raycastTarget = true;

			return img;
		}

		public Image MakeButton(Image img, ButtonConfig config) {
			img.gameObject.AddEntityComponent<Button>();
			img.gameObject.AddEntityComponent<ButtonMonitor>().SetConfig(config);
			this.EntityList().AddEntity(img.gameObject);

			//new to unity is the Raycast target. This much be set to true for buttons to work
			img.raycastTarget = true;

			return img;
		}

		public Tuple<GameObject, Text> MakeTextButton(string imagePath, GameObject parent, ButtonConfig buttonConfig, TextStyle style) {

			var button = MakeButton(imagePath, parent, buttonConfig);
			var text = MakeTextField("Label", button, style);

			return new Tuple<GameObject, Text>(button, text);
		}

		public Tuple<GameObject, Text> MakeTextButton(string imagePath, GameObject parent, Signal click, TextStyle style) {

			var button = MakeButton(imagePath, parent, new ButtonConfig(){click = click});
			var text = MakeTextField("Label", button, style);

			return new Tuple<GameObject, Text>(button, text);
		}

		public Tuple<GameObject, Text> MakeTextButton(string imagePath, GameObject parent, ButtonConfig buttonConfig) {

			var button = MakeButton(imagePath, parent, buttonConfig);
			var text = MakeTextField("Label", button);

			return new Tuple<GameObject, Text>(button, text);
		}

		public Tuple<GameObject, Text> MakeTextButton(string imagePath, GameObject parent, Signal click) {

			var button = MakeButton(imagePath, parent, new ButtonConfig(){click = click});
			var text = MakeTextField("Label", button);

			return new Tuple<GameObject, Text>(button, text);
		}

		public Text MakeTextField(string name, GameObject parent, TextStyle style) {

			var container = MakeContainer(name, parent);
			var localisedTxt = container.AddEntityComponent<LocalisedTextComponent>();
			localisedTxt.key = style.key;

			var txt = container.AddEntityComponent<Text>();

			//if no font is specified use the built in Arial font
			if(!string.IsNullOrEmpty(style.fontName)){
				txt.font = Resources.Load<Font>(style.fontName);
			}
			else{
				txt.font = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;
			}

			if(!string.IsNullOrEmpty(style.fontMaterial)){
				txt.material = Resources.Load<Material>(style.fontMaterial);
			}

			txt.fontStyle = style.fontStyle;
			txt.alignment = style.alignment;
			txt.fontSize = style.fontSize;
			txt.color = style.color;
			txt.resizeTextForBestFit = style.bestFit;
			txt.text = style.text;
			txt.verticalOverflow = style.verticalOverflow;
			txt.horizontalOverflow = style.horizontalOverflow;

			if(style.bestFit){
				txt.resizeTextMaxSize = style.maxSize;
				txt.resizeTextMinSize = style.minSize;
			}

			return txt;
		}

		public Text MakeTextField(string name, GameObject parent) {

			var container = MakeContainer(name, parent);
			container.AddEntityComponent<LocalisedTextComponent>();
			var txt = container.AddEntityComponent<Text>();

			return txt;
		}

		public T MakeTextField<T>(string name, GameObject parent, TextStyle style) where T : Component {
			var component = MakeTextField(name, parent, style).gameObject.AddEntityComponent<T>();
			return component;
		}

		public T MakeTextField<T>(string name, GameObject parent) where T : Component {
			var component = MakeTextField(name, parent).gameObject.AddEntityComponent<T>();
			return component;
		}

		public InputField MakeInputField(string name, GameObject parent, TextStyle textStyle, TextStyle placeholderStyle) {

			var container = MakeContainer(name, parent);
			var input = container.AddEntityComponent<InputField>();
			input.textComponent = MakeTextField("Text", container, textStyle);
			input.placeholder = MakeTextField("Placeholder", container, placeholderStyle);

			return input;
		}

		public InputField MakeInputField(string name, GameObject parent) {

			var container = MakeContainer(name, parent);
			var input = container.AddEntityComponent<InputField>();
			input.textComponent = MakeTextField("Text", container);
			input.placeholder = MakeTextField("Placeholder", container);

			return input;
		}

		public T MakeInputField<T>(string name, GameObject parent, TextStyle textStyle, TextStyle placeholderStyle) where T : Component {
			var component = MakeInputField(name, parent, textStyle, placeholderStyle).gameObject.AddEntityComponent<T>();
			return component;
		}

		public T MakeInputField<T>(string name, GameObject parent) where T : Component {
			var component = MakeInputField(name, parent).gameObject.AddEntityComponent<T>();
			return component;
		}

		public ScrollRect MakeScrollView(string name, GameObject parent) {

			var container = MakeContainer(name, parent);
			var scrollRect = container.AddEntityComponent<ScrollRect>();
			var viewPort = MakeContainer("ViewPort", container);
			var mask = viewPort.AddEntityComponent<Mask>();
			mask.showMaskGraphic = false;
			var img = viewPort.AddEntityComponent<Image>();
			img.color = Color.black;
			var content = MakeContainer("Content", viewPort);
			content.RectTransform().SetOffsetZero().SetAnchorMinMax(0,1,1,1).SetPivot(0f,1f);

			scrollRect.viewport = viewPort.RectTransform();
			scrollRect.viewport.SetPivot(0f,1f);

			scrollRect.content = content.RectTransform();

			return scrollRect;
		}

		public T MakeScrollView<T>(string name, GameObject parent) where T : Component {

			var component = MakeScrollView(name, parent).gameObject.AddEntityComponent<T>();
			return component;
		}
	}
}
