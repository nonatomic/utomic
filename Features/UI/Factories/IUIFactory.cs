//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using UnityEngine.UI;
using System;

namespace Nonatomic.Utomic {

	public interface IUIFactory{

		GameObject MakeContainer(string name, GameObject parent);
		T MakeContainer<T>(string name, GameObject parent) where T : Component;
		Image MakeImage(string imagePath, GameObject parent);
		T MakeImage<T>(string imagePath, GameObject parent) where T : Component;
		Text MakeTextField(string name, GameObject parent, TextStyle style);
		Text MakeTextField(string name, GameObject parent);
		T MakeTextField<T>(string name, GameObject parent, TextStyle style) where T : Component;
		T MakeTextField<T>(string name, GameObject parent) where T : Component;
		GameObject MakeButton(string imagePath, GameObject parent, ButtonConfig buttonConfig);
		GameObject MakeButton(string imagePath, GameObject parent, Signal click);
		Image MakeButton(Image img, Signal click);
		Image MakeButton(Image img, ButtonConfig config);
		Tuple<GameObject, Text> MakeTextButton(string imagePath, GameObject parent, ButtonConfig buttonConfig, TextStyle style);
		Tuple<GameObject, Text> MakeTextButton(string imagePath, GameObject parent, Signal click, TextStyle style);
		Tuple<GameObject, Text> MakeTextButton(string imagePath, GameObject parent, ButtonConfig buttonConfig);
		Tuple<GameObject, Text> MakeTextButton(string imagePath, GameObject parent, Signal click);
		InputField MakeInputField(string name, GameObject parent, TextStyle textStyle, TextStyle placeholderStyle);
		InputField MakeInputField(string name, GameObject parent);
		T MakeInputField<T>(string name, GameObject parent, TextStyle textStyle, TextStyle placeholderStyle) where T : Component;
		T MakeInputField<T>(string name, GameObject parent) where T : Component;
		ScrollRect MakeScrollView(string name, GameObject parent);
		T MakeScrollView<T>(string name, GameObject parent) where T : Component;
	}
}
