//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using UnityEngine.UI;
using System;

namespace Nonatomic.Utomic {

	public class UIUtils : IUIUtils {

		public Outline AddOutline(GameObject go, OutlineConfig config){

			var outline = go.AddEntityComponent<Outline>();
			outline.effectColor = config.outlineColor;
			outline.effectDistance = config.outlineDistance;
			outline.useGraphicAlpha = config.outlineUseAlpha;

			return outline;
		}

		public RectTransform AlterRectTransform(GameObject go, RectTransformConfig config){

			var rect = (RectTransform) go.transform;
			rect.anchorMin = config.anchorMin;
			rect.anchorMax = config.anchorMax;
			rect.offsetMin = config.offsetMin;
			rect.offsetMax = config.offsetMax;
			rect.pivot = config.pivot;
			rect.localScale = config.scale;
			rect.rotation = config.rotation;

			if(!config.anchoredPosition.Equals(Vector3.zero)){
				rect.anchoredPosition = config.anchoredPosition;
			}

			if(!config.sizeDelta.Equals(Vector3.zero)){
				rect.sizeDelta = config.sizeDelta;
			}

			return rect;
		}
	}
}
