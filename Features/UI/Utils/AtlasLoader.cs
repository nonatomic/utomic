//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

namespace Nonatomic.Utomic {

	public class AtlasLoader : IAtlasLoader {

		private Dictionary<string, Sprite> spriteList = new Dictionary<string, Sprite>();

		public void LoadAtlas(string path){

			var sprites = Resources.LoadAll<Sprite>(path);
			foreach(Sprite sprite in sprites){
				spriteList[sprite.name] = sprite;
			}
		}

		public Sprite GetSprite(string name){
			if(spriteList.ContainsKey(name)){
				return spriteList[name];
			}

			return null;
		}

		public bool HasSprite(string name){
			return spriteList.ContainsKey(name);
		}
	}
}
