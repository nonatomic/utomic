//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//
using System.Collections.Generic;
using System;

namespace Nonatomic.Utomic {

	/**
	* MenuState defines the state of a particular menu
	*/
	public abstract class MenuState {

		/**
		* hide unlisted menus
		*/
		public bool hideUnlisted = true;
		public List<Type> show = new List<Type>();
		public List<Type> hide = new List<Type>();

		protected void ShowMenu<T>() where T : IMenu {
			show.Add(typeof(T));
		}

		protected void HideMenu<T>() where T : IMenu {
			hide.Add(typeof(T));
		}
	}
}
