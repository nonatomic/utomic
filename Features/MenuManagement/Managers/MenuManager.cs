//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//
using System.Collections;
using System.Collections.Generic;

namespace Nonatomic.Utomic {

	public class MenuManager : IMenuManager {

		private Stack breadcrumbs = new Stack();
		private List<IMenu> menus = new List<IMenu>();

		public void ChangeState(MenuState state) {
			breadcrumbs.Push(state);

			foreach(IMenu menu in menus){
				if(state.show.Contains(menu.GetType())){
					menu.Show(state);
				}
				else if(state.hideUnlisted || state.hide.Contains(menu.GetType())){
					menu.Hide(state);
				}
			}
		}
		
		public MenuState CurrentState() {
			return (MenuState)breadcrumbs.Peek();
		}

		public void PreviousState() {

			if(breadcrumbs.Count < 2)
				return;

			breadcrumbs.Pop(); //current state
			ChangeState((MenuState) breadcrumbs.Pop()); //previous state
		}

		public void AddMenu(IMenu menu) {
			menus.Add(menu);
		}
	}
}
