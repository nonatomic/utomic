//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using System.Collections.Generic;
using System;

namespace Nonatomic.Utomic {

	//Error event
	public enum LocaliseError {
		MissingLanguage,
		MissingKey,
		ExceedsColumnLimit,
		FileNotFound,
		DuplicateKey
	}

	public class LocalisationManager : ILocalisationManager {

		//List fall back languages here - example if language code = SystemLanguage.ChineseTraditional
		// fall back to SystemLanguage.ChineseSimplified
		private Dictionary<SystemLanguage, SystemLanguage> languageFallback;
		private int keyColumn = 0;
		private Dictionary<SystemLanguage, int> languageColumns;
		private Dictionary<string, string[]> keyValueStore;

		private SystemLanguage language = SystemLanguage.English;
		public SystemLanguage Language {
			get { return language; }
		}

		public LocalisationManager(){
			languageColumns = new Dictionary<SystemLanguage, int>();
			languageFallback = new Dictionary<SystemLanguage, SystemLanguage>();
			keyValueStore = new Dictionary<string, string[]>();
		}

		public void AddLanguageFallback(SystemLanguage lang, SystemLanguage fallback){
			languageFallback.Add(lang, fallback);
		}

		//Fetch localisations by key word
		public string Localise(string key, string fallback) {

			if(!languageColumns.ContainsKey(language)) {

				if(!languageColumns.ContainsKey(language)) {
					this.Inject<LocalisationErrorSignal>().Dispatch(LocaliseError.MissingLanguage, "Error Missing Default Language:" + language.ToString());

					return fallback;
				}
			}

			if(!keyValueStore.ContainsKey(key)) {
				this.Inject<LocalisationErrorSignal>().Dispatch(LocaliseError.MissingKey,  "Error Missing Key:" + key);

				return "";
			}

			int langColumn = languageColumns[language];
			if(langColumn >= keyValueStore[key].Length) {
				this.Inject<LocalisationErrorSignal>().Dispatch(LocaliseError.ExceedsColumnLimit,  "Error Language " + language.ToString() + " column " + langColumn + " exceeds column limit. Column count = " + keyValueStore[key].Length);

				return "";
			}

			string val = keyValueStore[key][langColumn];

			//replace any new line characters
			val = val.Replace("\\n", "\n");

			return val;
		}

		//Load TSV
		public void LoadTSV(string tsvName) {

			//load tsv
			TextAsset tsv = Resources.Load(tsvName) as TextAsset;
			if(tsv == null) {
				this.Inject<LocalisationErrorSignal>().Dispatch(LocaliseError.FileNotFound, "Error TSV not found at Resources/" + tsvName);
			}

			//analyse the tsv
			string[] tsvLines = tsv.text.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
			// string[] tsvLines = tsv.text.Split('\n');

			if(keyValueStore == null){
				keyValueStore = new Dictionary<string, string[]>();
			}

			foreach (string line in tsvLines){
				string[] tabs = line.Split('\t');
				string key = tabs[keyColumn];
				keyValueStore[key] = tabs;
			}
		}

		public void SetLanguageColumn(SystemLanguage lang, int col) {

			if(languageColumns == null) {
				languageColumns = new Dictionary<SystemLanguage, int>();
			}

			if(languageColumns.ContainsKey(lang)) {
				this.Inject<LocalisationErrorSignal>().Dispatch(LocaliseError.DuplicateKey, "Error Language Key already registered:" + language.ToString());
				return;
			}

			languageColumns.Add(lang, col);
		}

		/**
		* Convenience method
		* Usage: Localise.Instance.SetLanguageColumns(2, new SystemLanguage[]{SystemLanguage.English, SystemLanguage.French});
		*/
		public void SetLanguageColumns(int startCol, SystemLanguage[] langs) {

			for(int i = 0; i < langs.Length; i++) {
				SetLanguageColumn(langs[i], startCol+i);
			}
		}

		public void SetKeyColumn(int keyCol) {
			keyColumn = keyCol;
		}

		public void SetLanguage(SystemLanguage lang) {

			if(language != lang) {

				//check if language exists
				if(languageColumns.ContainsKey(lang)){
					language = lang;
					this.Inject<LanguageChangeSignal>().Dispatch(language);
				}

				//check for language fallback
				else if(languageFallback.ContainsKey(lang)){

					if(language != languageFallback[lang]){
						language = languageFallback[lang];
						this.Inject<LanguageChangeSignal>().Dispatch(language);
					}
				}

				//else use default language
				else{

					if(language != SystemLanguage.English){
						language = SystemLanguage.English;
						this.Inject<LanguageChangeSignal>().Dispatch(language);
					}

				}
			}
		}

		public void UpdateText(){
			this.Inject<LanguageChangeSignal>().Dispatch(language);
		}

		public void Destroy(){
			keyColumn = 0;
			languageColumns = null;
			keyValueStore = null;
			language = SystemLanguage.English;
		}
	}
}
