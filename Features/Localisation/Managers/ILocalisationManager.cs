//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;

namespace Nonatomic.Utomic {

	public interface ILocalisationManager {

		string Localise(string key, string fallback);
		void LoadTSV(string tsvName);
		void SetLanguageColumn(SystemLanguage lang, int col);
		void SetLanguageColumns(int startCol, SystemLanguage[] langs);
		void SetKeyColumn(int keyCol);
		void SetLanguage(SystemLanguage lang);
		void AddLanguageFallback(SystemLanguage lanaguage, SystemLanguage fallback);
		void UpdateText();
		void Destroy();
	}
}
