using Nonatomic.Utomic;

namespace Nonatomic.Utomic {

	public class LocalisationErrorSignal : Signal {

		public LocalisationErrorSignal() {

			/**
			* @LocaliseError error type
			* @string message
			*/
			base.Restrict<LocaliseError, string>();
		}
	}
}
