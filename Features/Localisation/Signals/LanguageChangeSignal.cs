
using UnityEngine;

namespace Nonatomic.Utomic {

	public class LanguageChangeSignal : Signal {

		public LanguageChangeSignal() {

			/**
			* @SystemLanguage language
			*/
			base.Restrict<SystemLanguage>();
		}
	}
}
