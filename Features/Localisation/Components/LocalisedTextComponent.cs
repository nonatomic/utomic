//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace Nonatomic.Utomic {

	public class LocalisedTextComponent : MonoBehaviour {

		public string key;
		private List<TranslationData> dataList = new List<TranslationData>();

		public string SetText(string txt){
			return this.GetComponent<Text>().text = txt;
		}

		public void Translate(){

			var text = this.GetComponentInChildren<Text>();
			this.Log("KEY:" + key + ", TEXT:" + text);
			text.text = this.Inject<ILocalisationManager>().Localise(key, text.text);

			foreach(TranslationData data in dataList){
				text.text = text.text.Replace(data.searchString, data.data);
			}
		}

		public void SetData(List<TranslationData> data){
			dataList = data;
		}

		public void AddData(TranslationData data){
			dataList.Add(data);
		}
	}
}
