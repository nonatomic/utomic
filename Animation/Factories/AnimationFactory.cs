//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;

namespace Nonatomic.Utomic {

	public class AnimationFactory : IAnimationFactory {

		private ISpriteFactory spriteFactory;

		public AnimationFactory(){
			spriteFactory = this.Inject<ISpriteFactory>();
		}

		public GameObject MakeAnimation(string name, string animationControllerName, GameObject parent) {

			var animator = spriteFactory.MakeBlankSprite<Animator>(name, parent);
			animator.runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>(animationControllerName);

			return animator.gameObject;
		}
	}
}
