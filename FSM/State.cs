//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;

namespace Nonatomic.Utomic {

	public abstract class State {

		private List<ITransition> transitions = new List<ITransition>();
		private List<Type> backTriggers = new List<Type>();

		public State AddTransition<T, U>() where T:State where U:Trigger {
			var tran = new Transition();
			tran.To = typeof(T);
			tran.Trigger = typeof(U);
			transitions.Add(tran);
			return this;
		}

		public ITransition GetTransition<T>() where T:Trigger {
			var t = transitions.Where(x => x.Trigger == typeof(T)).ToList();

			if(t != null && t.Count > 0 )
				return t.First();

			return null;
		}

		public State AddBackTrigger<T>() where T:Trigger {
			backTriggers.Add(typeof(T));
			return this;
		}

		public bool HasBackTrigger<T>() where T:Trigger {
			return backTriggers.Contains(typeof(T));
		}

		public State RemoveBackTrigger<T>() where T:Trigger {
			var type = typeof(T);
			if(backTriggers.Contains(type))
				backTriggers.Remove(type);
			return this;
		}

		public virtual void Enter(GameObject go) {

		}

		public virtual void Exit(GameObject go) {

		}
	}
}
