//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Nonatomic.Utomic {

	public class FSM : MonoBehaviour {

		private List<State> states = new List<State>();
		private List<ITransition> transitions = new List<ITransition>();
		private Stack<State> breadcrumb = new Stack<State>();
		private State current;

		public State AddState<T>(bool startState = false) where T:State, new() {
			this.Log("AddState:" + typeof(T), Color.cyan);
			var t = new T();

			if(states.Contains(t))
				this.LogError(this + " already contains " + t);
			states.Add(t);

			if(startState)
				NextState(t);

			return t;
		}

		public void Trigger<T>() where T:Trigger{

			if(current == null)
				this.LogError("Attempting to trigger state with no default:" + this);

			var trans = current.GetTransition<T>() ?? GetTransition<T>();

			if(trans != null){

				foreach(State state in states){
					if(state.GetType() == trans.To) {
						current = state;
						state.Enter(gameObject);
						return;
					}
				}
			}

			if(current.HasBackTrigger<T>())
				PreviousState();

		}

		public FSM AddTransition<T, U>() where T:State where U:Trigger {
			var tran = new Transition();
			tran.To = typeof(T);
			tran.Trigger = typeof(U);
			transitions.Add(tran);
			return this;
		}

		public ITransition GetTransition<T>() where T:Trigger {
			var t = transitions.Where(x => x.Trigger == typeof(T)).ToList();

			if(t != null && t.Count > 0 )
				return t.First();

			return null;
		}

		private void NextState(State next){
			this.Log("NEXT A:" + next, Color.green);
			if(current != null){
				breadcrumb.Push(current);
				current.Exit(gameObject);
			}
			this.Log("NEXT C:" + next, Color.green);
			// current = next;
			this.Log("NEXT D:" + current + ", " + gameObject);
			// current.Enter(gameObject);
		}

		private void PreviousState(){

			if(breadcrumb.Count > 0){
				current.Exit(gameObject);
				current = breadcrumb.Pop();
				current.Enter(gameObject);
			}

		}
	}
}
