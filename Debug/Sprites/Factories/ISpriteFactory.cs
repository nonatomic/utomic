//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using System;
using System.Collections.Generic;

namespace Nonatomic.Utomic {

	public interface ISpriteFactory {

		GameObject MakeContainer(string name, GameObject parent);
		GameObject MakeSprite(string spriteName, GameObject parent);
		GameObject MakeBlankSprite(string spriteName, GameObject parent);
		T MakeSprite<T>(string spriteName, GameObject parent) where T:Component;
		T MakeBlankSprite<T>(string spriteName, GameObject parent) where T:Component;
		T MakeSpriteFromPool<T>(string spriteName, GameObject parent) where T:Component;
		T MakeSpriteFromPool<T>(string spriteName, GameObject parent, Predicate<T> search) where T:Component;
		GameObject MakeSpriteFromPool(string spriteName, GameObject parent, Predicate<GameObject> search);
		GameObject MakeSprite(string spriteName, Texture2D texture, GameObject parent);
	}
}
