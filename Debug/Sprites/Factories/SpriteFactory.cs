//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.UI;

namespace Nonatomic.Utomic {

	public class SpriteFactory : ISpriteFactory {

		public GameObject MakeContainer(string name, GameObject parent) {

			var container = new GameObject(name);
			container.transform.SetParent(parent.transform, false);

			this.EntityList().AddEntity(container);
			return container;
		}

		public GameObject MakeSprite(string spriteName, GameObject parent) {

			GameObject container;
			container = MakeContainer(spriteName, parent);
			container.transform.SetParent(parent.transform, false);
			SpriteRenderer sheetRenderer = container.AddEntityComponent<SpriteRenderer>();
			sheetRenderer.sprite = Resources.Load<Sprite>(spriteName);

			if(!string.IsNullOrEmpty(spriteName)){
				var atlasLoader = this.Inject<IAtlasLoader>();
				sheetRenderer.sprite = atlasLoader.GetSprite(spriteName) ?? Resources.Load<Sprite>(spriteName);
			}

			this.EntityList().AddEntity(container);

			return container;
		}

		public GameObject MakeBlankSprite(string spriteName, GameObject parent) {

			GameObject container;
			container = MakeContainer(spriteName, parent);
			container.transform.SetParent(parent.transform, false);
			container.AddEntityComponent<SpriteRenderer>();

			this.EntityList().AddEntity(container);

			return container;
		}

		public T MakeSprite<T>(string spriteName, GameObject parent) where T:Component {

			var sprite = MakeSprite(spriteName, parent);
			var component = sprite.AddEntityComponent<T>();

			return component;
		}

		public T MakeBlankSprite<T>(string spriteName, GameObject parent) where T:Component {

			var sprite = MakeBlankSprite(spriteName, parent);
			var component = sprite.AddEntityComponent<T>();

			return component;
		}

		public T MakeSpriteFromPool<T>(string spriteName, GameObject parent) where T:Component {

			var pool = this.Inject<IEntityPool>();
			var pooledSprite = pool.Pop<T>();
			T component = default(T);

			if(pooledSprite == null) {
				component = MakeSprite<T>(spriteName, parent);
			}
			else {
				component = pooledSprite.GetComponent<T>();
				pooledSprite.transform.SetParent(parent.transform, false);
			}

			this.EntityList().AddEntity(component.gameObject);
			return component;
		}

		/**
		* Pop an instance off the pool with a search term or make a new Sprite
		* returning T component
		*/
		public T MakeSpriteFromPool<T>(string spriteName, GameObject parent, Predicate<T> search) where T:Component {

			var pooledSprite = this.Inject<IEntityPool>().Pop<T>(search);
			T component = default(T);

			if(pooledSprite == null){
				component = MakeSprite<T>(spriteName, parent);
			}
			else{
				component = pooledSprite.GetComponent<T>();
				pooledSprite.transform.SetParent(parent.transform, false);
			}

			this.EntityList().AddEntity(component.gameObject);
			return component;
		}

		/**
		* Pop an instance off the pool with a search term or make a new GameObject
		* returning T component
		*/
		public GameObject MakeSpriteFromPool(string spriteName, GameObject parent, Predicate<GameObject> search){

			var pooledSprite = this.Inject<IEntityPool>().Pop(search);

			if(pooledSprite == null){
				pooledSprite = MakeSprite(spriteName, parent);
			}
			else{
				pooledSprite.transform.SetParent(parent.transform, false);
			}

			this.EntityList().AddEntity(pooledSprite);
			return pooledSprite;
		}

		public GameObject MakeSprite(string spriteName, Texture2D texture, GameObject parent) {

			var sprite = Sprite.Create(texture, new Rect(0,0,texture.width, texture.height), new Vector2(0.5f,0.5f), 1.0f);
			var container = MakeContainer(spriteName, parent);
			container.transform.SetParent(parent.transform, false);
			SpriteRenderer sheetRenderer = container.AddEntityComponent<SpriteRenderer>();
			sheetRenderer.sprite = sprite;

			this.EntityList().AddEntity(container);

			return container;
		}

	}
}
