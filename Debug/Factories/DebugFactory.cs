//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using UnityEngine.UI;

namespace Nonatomic.Utomic {

	public class DebugFactory : IDebugFactory {

		public DebugCanvas MakeCanvas(){

			var config = new CanvasConfig();
			config.name = "DebugCanvas";
			config.renderMode = RenderMode.ScreenSpaceOverlay;
			config.pixelPerfect = false;
			config.eventSystem = false;
			config.matchWidthOrHeight = 0.5f;

			var parent = this.EntityList().GetEntity<UtomicCore>();
			var canvas = this.Inject<ICanvasFactory>().Make<DebugCanvas>(config, parent);
			this.EntityList().AddComponent(canvas);
			return canvas;
		}

		public FPS MakeFPS() {

			var parent = this.EntityList().GetEntity<DebugCanvas>();

			var style = new TextStyle();
			style.bestFit = true;
			style.alignment = TextAnchor.MiddleLeft;

			var text = this.Inject<IUIFactory>().MakeTextField("FPS", parent, style);
			var fps = text.gameObject.AddEntityComponent<FPS>();

			var textRect = (RectTransform) text.gameObject.transform;
			textRect.anchorMin = new Vector2(0.01f, 0.96f);
			textRect.anchorMax = new Vector2(0.3f, 0.99f);

			this.EntityList().AddComponent(fps);
			return fps;
		}

		public EntityListCounter MakeEntityListCounter() {

			var parent = this.EntityList().GetComponent<DebugCanvas>().gameObject;

			var entityStyle = new TextStyle();
			entityStyle.color = Color.cyan;
			entityStyle.bestFit = true;
			entityStyle.alignment = TextAnchor.MiddleCenter;


			var componentStyle = new TextStyle();
			componentStyle.alignment = TextAnchor.MiddleCenter;
			componentStyle.color = Color.magenta;
			componentStyle.bestFit = true;

			var elc = parent.gameObject.AddEntityComponent<EntityListCounter>();

			elc.entitiesTxt = this.Inject<IUIFactory>().MakeTextField("Entity Counter", parent, entityStyle);
			var entitiesRect = (RectTransform) elc.entitiesTxt.gameObject.transform;
			entitiesRect.anchorMin = new Vector2(0.4f, 0.96f);
			entitiesRect.anchorMax = new Vector2(0.48f, 0.99f);

			elc.componentsTxt = this.Inject<IUIFactory>().MakeTextField("Component Counter", parent, componentStyle);
			var componentsRect = (RectTransform) elc.componentsTxt.gameObject.transform;
			componentsRect.anchorMin = new Vector2(0.52f, 0.96f);
			componentsRect.anchorMax = new Vector2(0.6f, 0.99f);

			this.EntityList().AddComponent(elc);
			return elc;
		}

		public MemoryDisplay MakeMemoryDisplay() {

			var parent = this.EntityList().GetComponent<DebugCanvas>().gameObject;

			var style = new TextStyle();
			style.alignment = TextAnchor.MiddleRight;
			style.color = Color.magenta;
			style.bestFit = true;

			var text = this.Inject<IUIFactory>().MakeTextField("Mem", parent, style);
			var textRect = (RectTransform) text.gameObject.transform;
			textRect.anchorMin = new Vector2(0.7f, 0.96f);
			textRect.anchorMax = new Vector2(0.99f, 0.99f);

			var memory = text.gameObject.AddEntityComponent<MemoryDisplay>();

			this.EntityList().AddComponent(memory);
			return memory;
		}
	}
}
