//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Nonatomic.Utomic {

	public class EntityListCounter : MonoBehaviour {

		public Text entitiesTxt;
		public Text componentsTxt;

		void Update() {

			if(entitiesTxt != null){
				entitiesTxt.text = "E:" + this.EntityList().EntityCount.ToString();
			}

			if(componentsTxt != null){
				componentsTxt.text = "C:" + this.EntityList().ComponentCount().ToString();
			}
		}
	}
}
