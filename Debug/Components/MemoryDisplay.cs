//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Nonatomic.Utomic {

	public class MemoryDisplay : MonoBehaviour {

		private Text text;

		void Start() {
			text = gameObject.GetComponent<Text>();
		}

		void Update() {

			text.text = "";

			#if ENABLE_PROFILER
				var textures = Resources.FindObjectsOfTypeAll(typeof(Texture));
				int bytes = 0;

				foreach(Texture t in textures){
					bytes += Profiler.GetRuntimeMemorySize(t);
				}

				double kb = bytes * 0.001;
				double mb = kb * 0.001;

				text.text = "M:" + mb.ToString("F2") + "mb";
			#endif

		}
	}
}
