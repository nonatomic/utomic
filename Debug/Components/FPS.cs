//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Nonatomic.Utomic {

	public class FPS : MonoBehaviour {

		private Text text;
		private float deltaTime = 0.0f;

		void Start() {
			text = gameObject.GetComponent<Text>();
		}

		void Update() {

			deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
			float msec = deltaTime * 1000.0f;
			float fps = 1.0f / deltaTime;
			text.text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);

			if(fps < 30){
				text.color = Color.yellow;
			}
			else{
				if(fps < 10){
					text.color = Color.red;
				}
				else{
					text.color = Color.green;
				}
			}
		}
	}
}
