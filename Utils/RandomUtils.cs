//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using System;

namespace Nonatomic.Utomic {

	public class RandomUtils : IRandomUtils {

		public string RandomLowerCaseLetter(){
			var r = new System.Random();
			int num = r.Next(0, 26);
			char letter = (char)('a' + num);
			return "" + letter;
		}

		public string RandomUpperCaseLetter(){
			return RandomLowerCaseLetter().ToUpper();
		}

		public T RandomEnum<T>(){
			
			Array values = Enum.GetValues(typeof(T));
			System.Random random = new System.Random();
			return (T)values.GetValue(random.Next(values.Length));
		}
	}
}
