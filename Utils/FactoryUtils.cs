//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;

namespace Nonatomic.Utomic {

	public class FactoryUtils : IFactoryUtils {


		public FactoryUtils(){

		}

		/**
		* Makes a GameObject
		* Adds the GameObject to the EntityList
		*/
		public GameObject MakeGameObject(string name) {
			var go = new GameObject(name);

			this.EntityList().AddEntity(go);
			return go;
		}

		/**
		* Makes a GameObject
		* Adds the GameObject to the EntityList
		*/
		public GameObject MakeGameObject(string name, Vector3 position, GameObject parent) {
			var go = MakeGameObject(name);
			go.transform.SetParent(parent.transform, false);
			go.transform.localPosition = position;
			return go;
		}

		/**
		* Makes a GameObject with a Single component atached
		* Adds the GameObject to the EntityList
		*/
		public T MakeGameObject<T>(string name, Vector3 position, GameObject parent) where T : MonoBehaviour {
			var go = MakeGameObject(name, position, parent);
			T component = go.AddEntityComponent<T>();

			this.EntityList().AddComponent(component);
			return component;
		}

		/**
		* Makes a GameObject with a Single component atached
		* Adds the GameObject to the EntityList
		*/
		public T MakeGameObject<T>(string name) where T : MonoBehaviour {
			var go = MakeGameObject(name);
			T component = go.AddEntityComponent<T>();

			this.EntityList().AddComponent(component);
			return component;
		}

		/**
		* Instantiates a prefab at position
		* Adds the resulting GameObject to the EntityList
		*/
		public GameObject MakePrefab(string prefabPath, Vector3 position, GameObject parent) {

			var go = MakePrefab(prefabPath);
			go.transform.SetParent(parent.transform, false);
			go.transform.localPosition = position;
			return go;
		}

		/**
		* Instantiates a prefab
		* Adds the resulting GameObject to the EntityList
		*/
		public GameObject MakePrefab(string prefabPath, GameObject parent) {
			var go = MakePrefab(prefabPath);
			go.transform.SetParent(parent.transform, false);
			return go;
		}

		/**
		* Instantiates a prefab
		* Adds the resulting GameObject to the EntityList
		*/
		public GameObject MakePrefab(string prefabPath) {

			var go = MonoBehaviour.Instantiate(Resources.Load(prefabPath, typeof(GameObject))) as GameObject;
			this.EntityList().AddEntity(go);
			return go;
		}
	}
}
