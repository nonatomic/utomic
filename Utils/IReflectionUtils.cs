//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using System.Reflection;
using UnityEngine;
using System;

namespace Nonatomic.Utomic {

	public interface IReflectionUtils {

		bool HasScriptAttribute(Component component, Type attribute);
		bool HasMethodAttribute(Component component, Type attribute);
		T GetField<T>(object obj, string field);
		void SetField(object obj, string field, object value);
	}
}
