//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using System.Reflection;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace Nonatomic.Utomic {

	public class ReflectionUtils : IReflectionUtils {

		/**
		*   Used to find script meta data Attribute like RequiresComponent
		*/
		public bool HasScriptAttribute(Component component, Type attribute){
			return component.GetType().IsDefined(attribute, true);
		}

		/**
		*   Used to find method meta data Attributes like ExposeToEditor
		*/
		public bool HasMethodAttribute(Component component, Type attribute){
			List<MethodInfo> methods = new List<MethodInfo>(attribute.GetMethods());

			foreach (MethodInfo mi in methods)
			{
				if (mi.IsDefined(attribute, true))
				{
					return true;
				}
			}

			return false;
		}

		/**
		*   Get T value from field by name on obj
		*/
		public T GetField<T>(object obj, string field) {
			Type type = obj.GetType();
			FieldInfo fieldInfo = type.GetField(field);
			return (T)fieldInfo.GetValue(obj);
		}

		/**
		* Set field by name to value on obj
		*/
		public void SetField(object obj, string field, object value) {
			PropertyInfo prop = obj.GetType().GetProperty(field, BindingFlags.Public | BindingFlags.Instance);
			if(prop != null && prop.CanWrite)
			{
				prop.SetValue(obj, value, null);
			}
			else{
				this.Log("Error: Cannot set field " + field + " on object type " + obj.GetType());
			}
		}
	}
}
