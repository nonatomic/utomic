//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using System;
using System.IO;

namespace Nonatomic.Utomic {

	public class ScreenshotUtils : IScreenshotUtils {

		public Texture2D TakeScreenshot(int width, int height, Camera screenshotCamera) {
			if(width<=0 || height<=0) return null;
			if(screenshotCamera == null) screenshotCamera = Camera.main;

			Texture2D screenshot = new Texture2D(width, height, TextureFormat.RGB24, false);
			RenderTexture renderTex = new RenderTexture(width, height, 24);
			screenshotCamera.targetTexture = renderTex;
			screenshotCamera.Render();
			RenderTexture.active = renderTex;
			screenshot.ReadPixels(new Rect(0, 0, width, height), 0, 0);
			screenshot.Apply(false);
			screenshotCamera.targetTexture = null;
			RenderTexture.active = null;
			GameObject.Destroy(renderTex);
			return screenshot;
		}

		public Texture2D TakeScreenshot(int width, int height, Camera screenshotCamera, string saveToFileName) {
			Texture2D screenshot = TakeScreenshot(width, height, screenshotCamera);
			if(screenshot != null && saveToFileName!=null) {
				if(Application.platform==RuntimePlatform.OSXPlayer ||
				Application.platform==RuntimePlatform.WindowsPlayer &&
				Application.platform!=RuntimePlatform.LinuxPlayer
				|| Application.isEditor) {
					byte[] bytes;
					if(saveToFileName.ToLower().EndsWith(".jpg"))
						bytes = screenshot.EncodeToJPG();
					else bytes = screenshot.EncodeToPNG();
					FileStream fs = new FileStream(saveToFileName, FileMode.OpenOrCreate);
					BinaryWriter w = new BinaryWriter(fs);
					w.Write(bytes);
					w.Close();
					fs.Close();
				}
			}
			return screenshot;
		}

		public void AddScreenshotToForm(int width, int height, Camera screenshotCamera, WWWForm uploadForm, string fieldName, string extension) {
			Texture2D screenshot = TakeScreenshot(width, height, screenshotCamera);
			if(screenshot != null && fieldName!=null && uploadForm!=null) {
				byte[] bytes;
				string mimeType;
				if(extension.ToLower()==("jpg")) {
					bytes = screenshot.EncodeToJPG();
					mimeType = "image/jpeg";
				} else {
					bytes = screenshot.EncodeToPNG();
					mimeType = "image/png";
		        }
		        uploadForm.AddBinaryData(fieldName, bytes, fieldName+"."+extension, mimeType);
			}
		}
	}
}
