//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using System;
using System.Collections.Generic;

namespace Nonatomic.Utomic {

	public class ChanceEngine<T> {

		private System.Random random;

		private struct Chance
		{
			public T Id { get; set; }
			public int Perc { get; set; }
		}
		private List<Chance> chances;
		private T fallBack;

		public ChanceEngine(T defaultId){
			fallBack = defaultId;
			chances = new List<Chance>();
			random = new System.Random();
		}

		/**
		* @int id = the id for the object to choose
		* @int chance = the chance 0-100 of the id being picked
		*/
		public void Add(T id, int chance) {
			Chance c = new Chance();
			c.Id = id;
			c.Perc = chance;

			chances.Add(c);
		}

		public T Run(){

			var possibles = new List<Chance>();
			foreach(Chance c in chances){

				if(random.Next(0,100) < c.Perc){
					possibles.Add(c);
				}
			}

			if(possibles.Count == 0){
				return fallBack;
			}

			return possibles[random.Next(possibles.Count)].Id;
		}
	}
}
