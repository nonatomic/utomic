//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using System;

namespace Nonatomic.Utomic {

	public interface IImageEffectsUtils {

		Texture2D ChangeTextureColor(Texture2D originalTexture, float deltaHue, float deltaSaturation, float deltaBrightness);
		Texture2D ChangeTextureContrast(Texture2D originalTexture, float power);
		Texture2D ChangeTextureContrastLinear(Texture2D originalTexture, float contrast);
		Texture2D CropTexture(Texture2D originalTexture, Rect cropRect);
		Texture2D Pixelate(Texture2D originalTexture, int pixelSize);
		Texture2D PixelateRegion(Texture2D originalTexture, int pixelSize, Rect region);
		Texture2D MirrorTexture(Texture2D originalTexture, bool horizontal, bool vertical);
		Texture2D GetEmptyTexture(int w, int h, Color color);
		Texture2D GetHSTexture(int width, int height, float brightness);
		Texture2D GetHBTexture(int width, int height, float saturation);
		Texture2D GetSBTexture(int width, int height, float hue);
		Texture2D GetHTexture(int width, int height, float saturation, float brightness);
		Texture2D GetSTexture(int width, int height, float hue, float brightness);
		Texture2D GetBTexture(int width, int height, float hue, float saturation);
		Texture2D GetATexture(int width, int height, Color color);
	}
}
