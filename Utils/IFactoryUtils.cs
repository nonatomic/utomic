//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;

namespace Nonatomic.Utomic {

	public interface IFactoryUtils {

		GameObject MakeGameObject(string name);
		GameObject MakeGameObject(string name, Vector3 position, GameObject parent);
		T MakeGameObject<T>(string name, Vector3 position, GameObject parent) where T : MonoBehaviour;
		T MakeGameObject<T>(string name) where T : MonoBehaviour;
		GameObject MakePrefab(string prefabPath, Vector3 position, GameObject parent);
		GameObject MakePrefab(string prefabPath, GameObject parent);
		GameObject MakePrefab(string prefabPath);
	}
}
