//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using System;
using System.Collections.Generic;

namespace Nonatomic.Utomic {

	public enum KeyState{
		HeldDown,
		Down,
		Up
	}

	public class KeyMap : MonoBehaviour{

		//map type of press to Input method used
		private Dictionary<KeyState, Func<KeyCode, bool>> methodMap = new Dictionary<KeyState, Func<KeyCode, bool>>()
		{
			{ KeyState.HeldDown, Input.GetKey},
			{ KeyState.Down, Input.GetKeyDown},
			{ KeyState.Up, Input.GetKeyUp}
		};

		private Dictionary<KeyState, Dictionary<KeyCode, ICommand>> commandMap = new Dictionary<KeyState, Dictionary<KeyCode, ICommand>>();
		private Dictionary<KeyState, Dictionary<KeyCode, Signal>> signalMap = new Dictionary<KeyState, Dictionary<KeyCode, Signal>>();

		public void Add(KeyCode keyCode, KeyState keyState, ICommand command){
			if(!commandMap.ContainsKey(keyState)){
				commandMap.Add(keyState, new Dictionary<KeyCode, ICommand>());
			}

			commandMap[keyState].Add(keyCode, command);
		}

		public void Add(KeyCode keyCode, KeyState keyState, Signal signal){
			if(!signalMap.ContainsKey(keyState)){
				signalMap.Add(keyState, new Dictionary<KeyCode, Signal>());
			}

			signalMap[keyState].Add(keyCode, signal);
		}

		public void Add(KeyCode[] keyCodes, KeyState keyState, ICommand command){
			foreach(KeyCode key in keyCodes){
				Add(key, keyState, command);
			}
		}

		public void Add(KeyCode[] keyCodes, KeyState keyState, Signal signal){
			foreach(KeyCode key in keyCodes){
				Add(key, keyState, signal);
			}
		}

		public void Reset() {
			commandMap.Clear();
			signalMap.Clear();
		}

		public void Remove(KeyCode keyCode, KeyState keyState){
			commandMap[keyState].Remove(keyCode);
			signalMap[keyState].Remove(keyCode);
		}

		public void Update(){

			foreach(KeyValuePair<KeyState, Dictionary<KeyCode, ICommand>> stateMap in commandMap){
				foreach(KeyValuePair<KeyCode, ICommand> keyMap in stateMap.Value ){
					if(methodMap[stateMap.Key](keyMap.Key)){
						keyMap.Value.Execute();
					}
				}
			}

			foreach(KeyValuePair<KeyState, Dictionary<KeyCode, Signal>> stateMap in signalMap){
				foreach(KeyValuePair<KeyCode, Signal> keyMap in stateMap.Value ){
					if(methodMap[stateMap.Key](keyMap.Key)){
						keyMap.Value.Dispatch();
					}
				}
			}
		}
	}
}
