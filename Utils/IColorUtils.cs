//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;

namespace Nonatomic.Utomic {

	public interface IColorUtils {

		Color ChangeBrightness(Color color, float correctionFactor);
		Vector3 MakeHSB(Color c);
		Vector4 MakeHSBA(Color c);
		string HexString(Color aColor);
		string HexString(Color aColor, bool includeAlpha);
		string HexString(Color32 aColor, bool includeAlpha);
		Color MakeColor(string aStr);
		Color MakeColor(Vector3 hsb);
		Color MakeColor(Vector4 hsba);
	}
}
