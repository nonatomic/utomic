//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using System;

namespace Nonatomic.Utomic {

	public class ImageEffectsUtils : IImageEffectsUtils {

		public Texture2D ChangeTextureColor(Texture2D originalTexture, float deltaHue, float deltaSaturation, float deltaBrightness) {

			var colorUtils = this.Inject<IColorUtils>();

			Texture2D newTexture = new Texture2D(originalTexture.width, originalTexture.height, TextureFormat.RGBA32, false);
			Color[] originalPixels = originalTexture.GetPixels(0);
			Color[] newPixels = newTexture.GetPixels(0);
			for (int i = 0; i < originalPixels.Length; i++) {
				Vector4 hsba = colorUtils.MakeHSBA(originalPixels[i]);
				hsba.x += deltaHue;
				hsba.y += deltaSaturation;
				hsba.z += deltaBrightness;
				newPixels[i] = colorUtils.MakeColor(hsba);
			}
			newTexture.SetPixels(newPixels, 0);
			newTexture.Apply();
			return newTexture;
		}

		//Remeber that the region origin is bottom left
		public Texture2D PixelateRegion(Texture2D originalTexture, int pixelSize, Rect region){

			int width = (int)region.width;
			int height = (int)region.height;

			int endX = (int)(region.x + region.width);
			int endY = (int)(region.y + region.height);

			if(endX > originalTexture.width){
				width = (int)(originalTexture.width - region.x);
			}

			if(endY > originalTexture.height){
				height = (int)(originalTexture.height - region.y);
			}

			int nearestWidth = (int)Math.Floor((width / (double)pixelSize)) * pixelSize;
			int nearestHeight = (int)Math.Floor((height / (double)pixelSize)) * pixelSize;
			int nearestX = (int)Math.Floor((region.x / (double)pixelSize)) * pixelSize;
			int nearestY = (int)Math.Floor((region.y / (double)pixelSize)) * pixelSize;
			this.Log("SIZE: " + originalTexture.width + ", " + originalTexture.height);
			this.Log("REGION: " + region.x + ", " + region.y + " size:" + width + ", " + height);

			Texture2D newTexture = new Texture2D(originalTexture.width, originalTexture.height);
			newTexture.SetPixels(originalTexture.GetPixels(0, 0, originalTexture.width, originalTexture.height));

			for(int x = (int)region.x; x < (nearestX + nearestWidth); x += pixelSize ){
				for(int y = (int)region.y; y < (nearestY + nearestHeight); y += pixelSize){
					Color[] pix = originalTexture.GetPixels(x, y, pixelSize, pixelSize);

					int total = pix.Length;
					var r = 0f;
					var g = 0f;
					var b = 0f;

					foreach(Color c in pix){
						r += c.r;
						g += c.g;
						b += c.b;

					}

					var averageColor = new Color((r / total), (g / total), (b / total));

					for(int i = 0; i < total; i++){
						pix[i] = averageColor;
					}

					newTexture.SetPixels(x,y,pixelSize,pixelSize,pix);

				}
			}

			newTexture.Apply();
			return newTexture;
		}

		public Texture2D Pixelate(Texture2D originalTexture, int pixelSize){

			int width = originalTexture.width;
			int height = originalTexture.height;
			int nearestWidth = (int)Math.Floor((width / (double)pixelSize)) * pixelSize;
			int nearestHeight = (int)Math.Floor((height / (double)pixelSize)) * pixelSize;
			this.Log("SIZE: " + width + ", " + height + " Nearest:" + nearestWidth + ", " + nearestHeight);

			Texture2D newTexture = new Texture2D(nearestWidth, nearestHeight);

			for(int x = 0; x < nearestWidth; x += pixelSize ){
				for(int y = 0; y < nearestHeight; y += pixelSize){
					Color[] pix = originalTexture.GetPixels(x, y, pixelSize, pixelSize);

					int total = pix.Length;
					var r = 0f;
					var g = 0f;
					var b = 0f;

					foreach(Color c in pix){
						r += c.r;
						g += c.g;
						b += c.b;

					}

					var averageColor = new Color((r / total), (g / total), (b / total));

					for(int i = 0; i < total; i++){
						pix[i] = averageColor;
					}

					newTexture.SetPixels(x,y,pixelSize,pixelSize,pix);

				}
			}

			newTexture.Apply();
			return newTexture;
		}

		public Texture2D ChangeTextureContrast(Texture2D originalTexture, float power) {

			if(power<0f) power=1f;
			Texture2D newTexture = new Texture2D(originalTexture.width, originalTexture.height, TextureFormat.RGBA32, false);
			Color[] originalPixels = originalTexture.GetPixels(0);
			Color[] newPixels = newTexture.GetPixels(0);
			float[] avgColor = new float[3];
			for (int i = 0; i < originalPixels.Length; i++) {
				Color c = originalPixels[i];
				avgColor[0]+=c.r;
				avgColor[1]+=c.g;
				avgColor[2]+=c.b;
			}
			avgColor[0] = avgColor[0] / originalPixels.Length;
			avgColor[1] = avgColor[1] / originalPixels.Length;
			avgColor[2] = avgColor[2] / originalPixels.Length;

			for (int i = 0; i < originalPixels.Length; i++) {
				Color c = originalPixels[i];
				float deltaR = c.r - avgColor[0];
				float deltaG = c.g - avgColor[1];
				float deltaB = c.b - avgColor[2];
				deltaR = Mathf.Pow(Mathf.Abs(deltaR), power) * Mathf.Sign(deltaR);
				deltaG = Mathf.Pow(Mathf.Abs(deltaG), power) * Mathf.Sign(deltaG);
				deltaB = Mathf.Pow(Mathf.Abs(deltaB), power) * Mathf.Sign(deltaB);
				newPixels[i] = new Color(avgColor[0] + deltaR,
					avgColor[1] + deltaG,
					avgColor[2] + deltaB,
					c.a);
			}
			newTexture.SetPixels(newPixels, 0);
			newTexture.Apply();
			return newTexture;
		}

		public Texture2D ChangeTextureContrastLinear(Texture2D originalTexture, float contrast) {

			Texture2D newTexture = new Texture2D(originalTexture.width, originalTexture.height, TextureFormat.RGBA32, false);
			Color[] originalPixels = originalTexture.GetPixels(0);
			Color[] newPixels = newTexture.GetPixels(0);
			float avgGrey = new float();
			for (int i = 0; i < originalPixels.Length; i++) {
				Color c = originalPixels[i];
				avgGrey+=c.r;
				avgGrey+=c.g;
				avgGrey+=c.b;
			}
			avgGrey = avgGrey / (3.0f * originalPixels.Length);

			for (int i = 0; i < originalPixels.Length; i++) {
				Color c = originalPixels[i];
				float deltaR = c.r - avgGrey;
				float deltaG = c.g - avgGrey;
				float deltaB = c.b - avgGrey;
				newPixels[i] = new Color(avgGrey + (deltaR * contrast),
					avgGrey + (deltaG * contrast),
					avgGrey + (deltaB * contrast),
					c.a);
			}
			newTexture.SetPixels(newPixels, 0);
			newTexture.Apply();
			return newTexture;
		}

		public Texture2D CropTexture(Texture2D originalTexture, Rect cropRect) {

			// Make sure the crop rectangle stays within the original Texture dimensions
			cropRect.x = Mathf.Clamp(cropRect.x, 0, originalTexture.width);
			cropRect.width = Mathf.Clamp(cropRect.width, 0, originalTexture.width - cropRect.x);
			cropRect.y = Mathf.Clamp(cropRect.y, 0, originalTexture.height);
			cropRect.height = Mathf.Clamp(cropRect.height, 0, originalTexture.height - cropRect.y);
			if(cropRect.height<=0 || cropRect.width<=0) return null; // dont create a Texture with size 0

			Texture2D newTexture = new Texture2D((int)cropRect.width, (int)cropRect.height, TextureFormat.RGBA32, false);
			Color[] pixels = originalTexture.GetPixels((int)cropRect.x, (int)cropRect.y, (int)cropRect.width, (int)cropRect.height, 0);
			newTexture.SetPixels(pixels);
			newTexture.Apply();
			return newTexture;
		}

		public Texture2D MirrorTexture(Texture2D originalTexture, bool horizontal, bool vertical) {
			Texture2D newTexture = new Texture2D(originalTexture.width, originalTexture.height, TextureFormat.RGBA32, false);;
			Color[] originalPixels = originalTexture.GetPixels(0);
			Color[] newPixels = newTexture.GetPixels(0);
			for (int y = 0; y < originalTexture.height; y++) {
				for (int x = 0; x < originalTexture.width; x++) {
					int newX = horizontal ? (newTexture.width-1-x) : x;
					int newY = vertical ? (newTexture.height-1-y) : y;
					newPixels[(newY * newTexture.width) + newX] = originalPixels[(y * originalTexture.width) + x];
				}
			}
			newTexture.SetPixels(newPixels, 0);
			newTexture.Apply();
			return newTexture;
		}

		public Texture2D GetEmptyTexture(int w, int h, Color color) {
			Texture2D img = new Texture2D(w, h, TextureFormat.RGBA32, false);
			Color[] pixels = img.GetPixels(0);
			for (int i = 0; i < pixels.Length; i++) {
				pixels[i] = color;
			}
			img.SetPixels(pixels, 0);
			img.Apply();
			return img;
		}

		public Texture2D GetHSTexture(int width, int height, float brightness) {

			var colorUtils = this.Inject<IColorUtils>();

			Texture2D img = new Texture2D(width, height, TextureFormat.RGBA32, false);
			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					Color p = colorUtils.MakeColor(new Vector3((float)x/width, (float)y/height, brightness));
					img.SetPixel(x, y, p);
				}
			}
			img.Apply();
			return img;
		}

		public Texture2D GetHBTexture(int width, int height, float saturation) {

			var colorUtils = this.Inject<IColorUtils>();

			Texture2D img = new Texture2D(width, height, TextureFormat.RGBA32, false);
			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					Color p = colorUtils.MakeColor(new Vector3((float)x/width, saturation, (float)y/height));
					img.SetPixel(x, y, p);
				}
			}
			img.Apply();
			return img;
		}

		public Texture2D GetSBTexture(int width, int height, float hue) {

			var colorUtils = this.Inject<IColorUtils>();

			Texture2D img = new Texture2D(width, height, TextureFormat.RGBA32, false);
			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					Color p = colorUtils.MakeColor(new Vector3(hue, (float)x/width, (float)y/height));
					img.SetPixel(x, y, p);
				}
			}
			img.Apply();
			return img;
		}

		public Texture2D GetHTexture(int width, int height, float saturation, float brightness) {

			var colorUtils = this.Inject<IColorUtils>();
			Texture2D img = new Texture2D(width, height, TextureFormat.RGBA32, false);
			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					Color p = (width > height)
						? colorUtils.MakeColor(new Vector3((float)x/width, saturation, brightness))
						: colorUtils.MakeColor(new Vector3((float)y/height, saturation, brightness));
					img.SetPixel(x, y, p);
				}
			}
			img.Apply();
			return img;
		}

		public Texture2D GetSTexture(int width, int height, float hue, float brightness) {

			var colorUtils = this.Inject<IColorUtils>();
			Texture2D img = new Texture2D(width, height, TextureFormat.RGBA32, false);
			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					Color p = (width > height)
						? colorUtils.MakeColor(new Vector3(hue, (float)x/width, brightness))
						: colorUtils.MakeColor(new Vector3(hue, (float)y/height, brightness));
					img.SetPixel(x, y, p);
				}
			}
			img.Apply();
			return img;
		}

		public Texture2D GetBTexture(int width, int height, float hue, float saturation) {

			var colorUtils = this.Inject<IColorUtils>();
			Texture2D img = new Texture2D(width, height, TextureFormat.RGBA32, false);

			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					Color p = (width > height)
						? colorUtils.MakeColor(new Vector3(hue, saturation, (float)x/width))
						: colorUtils.MakeColor(new Vector3(hue, saturation, (float)y/height));
					img.SetPixel(x, y, p);
				}
			}
			img.Apply();
			return img;
		}

		public Texture2D GetATexture(int width, int height, Color color) {
			Texture2D img = new Texture2D(width, height, TextureFormat.RGBA32, false);
			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					Color p = color;
					p.a = (width > height)
						? (float)x/width
						: (float)y/height;
					img.SetPixel(x, y, p);
				}
			}
			img.Apply();
			return img;
		}
	}
}
