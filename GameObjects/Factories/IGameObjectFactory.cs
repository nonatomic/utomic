//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Nonatomic.Utomic {

	public interface IGameObjectFactory {


		/**
		* Makes and returns a GameObject whilst settings the parent
		*/
		GameObject Make(string name, GameObject parent);

		/**
		* Makes a GameObject and adds component T
		* returning T
 		*/
		T Make<T>(string name) where T:Component;

		/**
		* Makes a GameObject and adds component T
		* returning T component
 		*/
		T Make<T>(string name, GameObject parent) where T:Component;

		/**
		* Pop an instance off the pool or make a new GameObject
		* returning T component
		*/
		T MakeFromPool<T>(string name, GameObject parent) where T:Component;

		/**
		* Pop an instance off the pool with a search term or make a new GameObject
		* returning T component
		*/
		T MakeFromPool<T>(string name, GameObject parent, Predicate<T> search) where T:Component;

		/**
		* Pop an instance off the pool with a search term or make a new GameObject
		* returning T component
		*/
		GameObject MakeFromPool(string name, GameObject parent, Predicate<GameObject> search);
	}
}
