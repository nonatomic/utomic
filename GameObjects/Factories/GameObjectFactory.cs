//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Nonatomic.Utomic {

	public class GameObjectFactory : IGameObjectFactory  {


		/**
		* Makes and returns a GameObject whilst settings the parent
		*/
		public GameObject Make(string name, GameObject parent){
			var go = new GameObject(name);
			go.transform.SetParent(parent.transform, false);
			this.EntityList().AddEntity(go);
			return go;
		}

		/**
		* Makes a GameObject and adds component T
		* returning T
 		*/
		public T Make<T>(string name) where T:Component {
			var go = new GameObject(name);
			var component = go.AddEntityComponent<T>();
			this.EntityList().AddEntity(go);
			return component;
		}

		/**
		* Makes a GameObject and adds component T
		* returning T component
 		*/
		public T Make<T>(string name, GameObject parent) where T:Component{
			var go = new GameObject(name);
			go.transform.SetParent(parent.transform, false);
			var component = go.AddEntityComponent<T>();
			this.EntityList().AddEntity(go);
			return component;
		}

		/**
		* Pop an instance off the pool or make a new GameObject
		* returning T component
		*/
		public T MakeFromPool<T>(string name, GameObject parent) where T:Component{

			var pooledGo = this.Inject<IEntityPool>().Pop<T>();
			T component = default(T);

			if(pooledGo == null){
				component = Make<T>(name, parent);
			}
			else{
				component = pooledGo.GetComponent<T>();
			}

			this.EntityList().AddEntity(pooledGo);
			return component;
		}

		/**
		* Pop an instance off the pool with a search term or make a new GameObject
		* returning T component
		*/
		public T MakeFromPool<T>(string name, GameObject parent, Predicate<T> search) where T:Component{

			var pooledGo = this.Inject<IEntityPool>().Pop<T>(search);
			T component = default(T);

			if(pooledGo == null){
				component = Make<T>(name, parent);
			}
			else{
				component = pooledGo.GetComponent<T>();
			}

			this.EntityList().AddEntity(pooledGo);
			return component;
		}

		/**
		* Pop an instance off the pool with a search term or make a new GameObject
		* returning T component
		*/
		public GameObject MakeFromPool(string name, GameObject parent, Predicate<GameObject> search){

			var pooledGo = this.Inject<IEntityPool>().Pop(search);

			if(pooledGo == null){
				pooledGo = Make(name, parent);
			}

			this.EntityList().AddEntity(pooledGo);
			return pooledGo;
		}
	}
}
