//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;

namespace Nonatomic.Utomic {

	public class UtomicCore : MonoBehaviour {

		private void Awake() {

			this.Log("============ UTOMIC ============", Color.cyan);
			this.Log("=========== NEW GAME ===========", Color.magenta);

			/**
			* Application Settings
			*
			*/

			//Frame rate
			QualitySettings.vSyncCount = 0;
			Application.targetFrameRate = -1;

			/**
			* Core
			*
			* These are objects that are essential to the running of a typical
			* Utomic based game
			*/
			this.Bind<IEntityList, EntityList>();
			this.Bind<IEntityPool, EntityPool>();
			this.Bind<ICommandMap, CommandMap>();
			this.Bind<IService, Service>();
			this.Bind<ISceneLoader, SceneLoader>();

			/**
			* Factories
			*
			* These are responsible for making core objects
			*
			*/
			this.Bind<ISpriteFactory, SpriteFactory>();
			this.Bind<IAnimationFactory, AnimationFactory>();
			this.Bind<IUIFactory, UIFactory>();
			this.Bind<IPrefabFactory, PrefabFactory>();
			this.Bind<ICameraFactory, CameraFactory>();
			this.Bind<ICanvasFactory, CanvasFactory>();
			this.Bind<IGameObjectFactory, GameObjectFactory>();
			this.Bind<ILightingFactory, LightingFactory>();

			/**
			* Utilities
			*
			* These are collections of methods that aid processes and have no
			* wiring requirements.
			*/
			this.Bind<IFactoryUtils, FactoryUtils>();
			this.Bind<IReflectionUtils, ReflectionUtils>();
			this.Bind<IColorUtils, ColorUtils>();
			this.Bind<IRandomUtils, RandomUtils>();
			this.Bind<IImageEffectsUtils, ImageEffectsUtils>();
			this.Bind<IUIUtils, UIUtils>();
			this.Bind<IAtlasLoader, AtlasLoader>();

			/**
			* Features
			*
			* Any core manager or utility that requires wiring should be wrapped
			* as a feature and added here
			*
			* Features may include factories, models, signals, commands, managers etc..
			*/
			this.AddFeature<AudioFeature>();
			this.AddFeature<LocalisationFeature>();
			this.AddFeature<PreloadAssetsFeature>();
			this.AddFeature<MenuManagementFeature>();

			/**
			* Utility Components
			*
			* These are collections of methods that aid processes and have no
			* wiring requirements but have a dependancy on MonoBehaviour
			*/
			gameObject.AddEntityComponent<CoroutineRunner>();
			gameObject.AddEntityComponent<LanguageChangeMonitorComponent>();
			gameObject.AddEntityComponent<InvokeRunner>();

			//Keep a reference to the Utomic
			this.EntityList().AddEntity(gameObject);
		}

		public void Start() {

			//keep a reference to the UtomicCore
			this.EntityList().AddEntity(this.gameObject);

			this.Log("Structure Features", Color.cyan);
			this.StructureFeatures();

			this.Log("Setup Features", Color.cyan);
			this.SetupFeatures();

			//start preloading
			this.Log("Load Assets", Color.cyan);
			var preloaderComplete = this.Inject<PreloadAssetsCompleteSignal>();
			var preloaderProgress = this.Inject<PreloadAssetsProgressSignal>();
			this.SceneLoader().LoadAdditive("Assets", preloaderProgress, preloaderComplete);
		}
	}
}
