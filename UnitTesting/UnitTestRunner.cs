//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System;
using System.Reflection;
using System.Linq;

namespace Nonatomic.Utomic {

	public class UnitTestRunner : IUnitTestRunner {

		private Dictionary<IUnitTest, Queue<MethodInfo>> tests = new Dictionary<IUnitTest, Queue<MethodInfo>>();

		public void Add(IUnitTest unitTest){

			Type type = unitTest.GetType();
			MethodInfo[] methods = type.GetMethods(BindingFlags.Instance | BindingFlags.Public);

			foreach(MethodInfo methodInfo in methods){

				var attribute = Attribute.GetCustomAttribute(methodInfo,typeof(UnitTestAttribute));

				if(attribute != null && methodInfo.IsPublic && methodInfo.ReturnType == typeof(bool)){

					if(!tests.ContainsKey(unitTest)){
						tests[unitTest] = new Queue<MethodInfo>();
					}

					tests[unitTest].Enqueue(methodInfo);
				}
			}
		}

		public void Run(){

			this.Log("============UNIT TEST RUNNER ============", Color.magenta);

			Next();
		}

		private void Next(){
			if(tests.Count > 0){
				var key = tests.Keys.First();
				var queue = tests[key];

				if(queue.Count > 0){
					Test(key, queue.Dequeue());
				}
				else{
					tests.Remove(key);
					Next();
				}
			}
		}

		private void Test(IUnitTest unitTest, MethodInfo methodInfo){

			//clear the memory to provide a base for each test
			GC.Collect();

			//setup dependencies
			unitTest.Setup();


			Type type = unitTest.GetType();
			var result = false;
			var exception = false;

			try {
				result = (bool) methodInfo.Invoke(unitTest, null);
			}
			catch (Exception e) {
				this.Log("Fail: <color=yellow>" + type.Name + "</color> <color=orange>" + methodInfo.Name + "</color> " + e.Message, Color.red);
				exception = true;
			}

			//remove test products
			unitTest.TearDown();

			//report the result
			if(!exception){
				if(result){
					this.Log("Pass: <color=yellow>" + type.Name + "</color> <color=orange>" + methodInfo.Name + "</color>", Color.green);
				}
				else{
					this.Log("Fail: <color=yellow>" + type.Name + "</color> <color=orange>" + methodInfo.Name + "</color>", Color.red);
				}
			}


			if(tests.Count > 0){
				Next();
			}
			else{
				this.Log("===========================================", Color.magenta);
			}
		}
	}
}
