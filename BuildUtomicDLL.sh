UNITY_ENGINE="/Applications/Unity_5_3_3p3/Unity.app/Contents/Frameworks/Managed/UnityEngine.dll"
UNITY_EDITOR="/Applications/Unity_5_3_3p3/Unity.app/Contents/Frameworks/Managed/UnityEditor.dll"
UNITY_UI="/Applications/Unity_5_3_3p3/Unity.app/Contents/UnityExtensions/Unity/GUISystem/UnityEngine.UI.dll"
OUT="/Users/Nonatomic/Documents/Repos/300-Utomic/Utomic.dll"
UTOMICTEMPLATE="/Users/Nonatomic/Documents/Repos/000-UtomicTemplate/utomic-unity-template/UtomicTemplate/UtomicTemplate/Assets/Game/Scripts/Plugins/Utomic.dll"
TVS="/Users/Nonatomic/Documents/Repos/320-TVS/Client/TVS/Assets/App/Plugins/Utomic.dll"
# STABILIZE="/Users/Nonatomic/Documents/Repos/350-Stabilize/Stabilize/Stabilize/Assets/Game/Scripts/Plugins/Utomic.dll"
# KICKSTARTER="/Users/Nonatomic/Documents/Repos/360-Kickstarter/KickStarter/KickStarter/Assets/Game/Scripts/Plugins/Utomic.dll"
# GGJ="/Users/Nonatomic/Documents/Repos/390-GGJ16/Client/GGJ/Assets/Game/Plugins/Utomic.dll"

#build Utomic.dll library from all C# files
cd /Users/Nonatomic/Documents/Repos/300-Utomic
mcs -sdk:2 -r:$UNITY_ENGINE,$UNITY_UI,$UNITY_EDITOR -target:library -out:$OUT -recurse:"*.cs"

# copy Utomic.dll into dependant projects
cp $OUT $UTOMICTEMPLATE
cp $OUT $TVS
# cp $OUT $STABILIZE
# cp $OUT $KICKSTARTER
# cp $OUT $GGJ
