//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System;

namespace Nonatomic.Utomic {

	public class BenchMarkRunner : IBenchMarkRunner {

		private Queue<IBenchMark> benchMarks = new Queue<IBenchMark>();
		private const int ITERATIONS = 1000000;

		public void Add(IBenchMark benchMark){
			benchMarks.Enqueue(benchMark);
		}

		public void Run(){

			this.Log("============ BENCH MARK RUNNER ============", Color.magenta);

			if(benchMarks.Count > 0){
				Test(benchMarks.Dequeue());
			}
		}

		private void Test(IBenchMark benchMark){

			//clear the memory to provide a base for each test
			GC.Collect();

			//setup dependencies
			benchMark.Setup();

			//run once outside of loop to avoid initialization costs
			benchMark.Run();

			//begin timed test
			Stopwatch watch = Stopwatch.StartNew();
			for (int i = 0; i < ITERATIONS; i++){
				benchMark.Run();
			}
			watch.Stop();

			//remove test products
			benchMark.TearDown();

			float time = (float)watch.ElapsedMilliseconds / (float)ITERATIONS;
			this.Log("Bench Mark <color=yellow>" + benchMark + "</color> Complete: <color=yellow>" + time.ToString() + "</color> ms per call in " + ITERATIONS, Color.green);

			if(benchMarks.Count > 0){
				Test(benchMarks.Dequeue());
			}
			else{
				this.Log("===========================================", Color.magenta);
			}
		}
	}
}
