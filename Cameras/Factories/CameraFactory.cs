//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;

namespace Nonatomic.Utomic {

	public class CameraFactory : ICameraFactory {

		public Camera Make(CameraConfig config, GameObject parent) {

			var go = new GameObject(config.name);
			go.transform.SetParent(parent.transform);
			go.transform.localPosition = config.position;
			go.transform.eulerAngles = config.eulerAngles;
			go.tag = config.tag;

			var camera = go.AddEntityComponent<Camera>();
			camera.orthographic = config.orthographic;
			camera.orthographicSize = config.orthographicSize;
			camera.backgroundColor = config.color;
			camera.clearFlags = config.clearFlags;
			camera.nearClipPlane = config.nearClipPlane;
			camera.farClipPlane = config.farClipPlane;
			camera.cullingMask = config.cullingMask;
			camera.fieldOfView = config.fieldOfView;


			this.EntityList().AddEntity(go);
			return camera;
		}

		public T Make<T>(CameraConfig config, GameObject parent) where T : Component {
			T component = Make(config, parent).gameObject.AddEntityComponent<T>();
			this.EntityList().AddComponent(component);
			return component;
		}
	}
}
