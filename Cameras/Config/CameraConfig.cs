//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using System;

namespace Nonatomic.Utomic {

	public class CameraConfig {

		public string name = "Camera";
		public bool orthographic;
		public float orthographicSize;
		public Color color = Color.blue;
		public CameraClearFlags clearFlags = CameraClearFlags.Nothing;
		public float nearClipPlane = 0.3f;
		public float farClipPlane = 1000f;
		public int cullingMask = 0;
		public Vector3 position = Vector3.zero;
		public Vector3 eulerAngles = Vector3.zero;
		public string tag = "MainCamera";
		public float fieldOfView = 60f;
		
	}
}
