using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;

namespace Nonatomic.Utomic {

	public class InvokeRunner : MonoBehaviour {

		/**
		* This struct stores the details about the action
		* to be invoked and the duration to wait.
		*/
		struct Invocation{

			public Action method;
			public int duration;
			public int startTime;

			public Invocation(Action method, int duration){
				this.method = method;
				this.duration = duration;
				this.startTime = Environment.TickCount;
			}
		}

		private List<Invocation> invocations = new List<Invocation>();

		/**
		* Add a method to be called in x milliseconds
		*/
		public void Add(Action method, int delay = 0){

			if(delay == 0){
				method.Invoke();
			}
			else{
				invocations.Add(new Invocation(method, delay));
			}
		}

		/**
		* Remove the method from the invocation list
		*/
		public void Remove(Action method){
			invocations.RemoveAll(inv => inv.method == method);
		}

		/**
		* Resets the invocation list, removing all existing invocations
		*/
		public void Reset(){
			invocations.Clear();
		}

		/**
		* Iterates and executes invocations
		*/
		public void Update(){

			if(invocations.Count == 0)
				return;

			var currentTime = Environment.TickCount;
			int timeDiff;

			List<Invocation> currentInvocations = invocations.ToList();

			foreach(Invocation inv in currentInvocations){

				timeDiff = currentTime - inv.startTime;

				if(timeDiff >= inv.duration){
					inv.method.Invoke();
					invocations.Remove(inv);
				}
			}
		}
	}
}
