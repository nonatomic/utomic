using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

namespace Nonatomic.Utomic {

	[RequireComponent (typeof (Canvas))]
	public class CanvasDepthManager : MonoBehaviour {

		public void Awake() {
			Sort();
		}

		public void Sort(){

			List<DepthComponent> dcs = this.EntityList().GetComponents<DepthComponent>();
			DepthComponent dc;
			RectTransform t;

			for(int i = 0; i < dcs.Count; i++){

				dc = dcs[i];

				if(dc.value == -1)
					continue;

				if(dc.gameObject == this.gameObject)
					continue;

				t = (RectTransform) dc.gameObject.transform;
				int index = t.GetSiblingIndex();
				int count = t.parent.childCount;


				if(index < dc.value && index < count-1){

					var next = t.parent.GetChild(index + 1);
					var nextDepth = next.GetComponent<DepthComponent>();
					if(nextDepth != null && dc.value > nextDepth.value){
						t.SetSiblingIndex(next.GetSiblingIndex());
					}
				}
				else if(index > dc.value && index > 0){

					var prev = t.parent.GetChild(index - 1);
					var prevDepth = prev.GetComponent<DepthComponent>();
					if(prevDepth != null && dc.value < prevDepth.value){
						t.SetSiblingIndex(prev.GetSiblingIndex());
					}
				}
			}

			Invoke("Sort", 0.15f);
		}
	}
}
