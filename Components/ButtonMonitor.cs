using UnityEngine;
using UnityEngine.UI;
using Nonatomic.Utomic;
using UnityEngine.EventSystems;

namespace Nonatomic.Utomic {

	[RequireComponent (typeof (Button))]
	public class ButtonMonitor : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler, IPointerUpHandler, IPointerClickHandler {

		private ButtonConfig config;
		private Button btn;
		private bool downLocked;
		private bool clickLocked;
		private Image img;

		public void Start(){
			btn = gameObject.GetComponent<Button>();
		}

		public void SetConfig(ButtonConfig buttonConfig){
			config = buttonConfig;
		}

		public void OnPointerDown (PointerEventData eventData) {

			if(!btn.enabled)
				return;

			if(downLocked)
				return;

			DownLock();

			if(config != null && config.down != null) {
				this.Log (this.gameObject.name, "PointerDown", "green", "cyan");
				config.down.Dispatch(this.gameObject);
			}

			if(config != null)
					SwapSprite(config.downSprite);
		}

		public void SwapSprite(Sprite spr){

			if(spr == null)
				return;

			img = img ?? this.GetComponent<Image>();

			if(img == null)
				return;

			img.overrideSprite = spr;
		}

		public void OnPointerEnter (PointerEventData eventData) {

			if(!btn.enabled)
				return;

			if(config != null && config.enter != null) {
				this.Log (this.gameObject.name, "PointerEnter", "green", "cyan");
				config.enter.Dispatch(this.gameObject);
			}

			if(config != null)
					SwapSprite(config.enterSprite);
		}

		public void OnPointerExit(PointerEventData eventData) {

			if(!btn.enabled)
				return;

			if(config != null && config.exit != null) {
				this.Log (this.gameObject.name, "PointerExit", "green", "cyan");
				config.exit.Dispatch(this.gameObject);
			}

			if(config != null)
					SwapSprite(config.exitSprite);
		}

		public void OnPointerUp(PointerEventData eventData) {

			if(!btn.enabled)
				return;

			if(config != null && config.up != null) {
				this.Log (this.gameObject.name, "PointerUp", "green", "cyan");
				config.up.Dispatch(this.gameObject);
			}

			if(config != null)
					SwapSprite(config.upSprite);
		}

		public void OnPointerClick (PointerEventData eventData) {

			if(!btn.enabled)
				return;

			if(clickLocked)
				return;

			ClickLock();

			if(config != null && config.click != null) {
				this.Log (this.gameObject.name, "PointerClick", "green", "cyan");
				config.click.Dispatch(this.gameObject);
			}

			if(config != null)
					SwapSprite(config.clickSprite);
		}

		private void ClickLock(){
			clickLocked = true;
			Invoke("ClickUnlock", config.clickLockDuration);
		}

		private void ClickUnlock(){
			clickLocked = false;
		}

		private void DownLock(){
			downLocked = true;
			Invoke("DownUnlock", config.downLockDuration);
		}

		private void DownUnlock(){
			downLocked = false;
		}
	}
}
