using UnityEngine;
using System.Collections;
using System;

namespace Nonatomic.Utomic {

	public class CoroutineRunner : MonoBehaviour {

		public void Run(IEnumerator method) {
			StartCoroutine(method);
		}

		public void Kill(){
			StopAllCoroutines();
		}

		public void Wait(float duration, Action method){
			StartCoroutine(WaitRunner(duration, method));
		}

		public void EndOfFrame(Action method){
			StartCoroutine(EndFrameRunner(method));
		}

		private IEnumerator EndFrameRunner(Action method){
			yield return 0;
			method();
		}

		private IEnumerator WaitRunner(float duration, Action method){
			yield return new WaitForSeconds(duration);
			method();
		}
	}
}
