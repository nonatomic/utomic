using UnityEngine;
using System.Collections;
using System.Linq;

namespace Nonatomic.Utomic {

	/*
	* Whilst this component uses mouse input this works
	* on mobile devices also
	*/
	public class SwipeComponent : MonoBehaviour {

		private Vector2 startPos;
		public RectTransform area;

		//amount of movement required to trigger swipe
		public float moveX;
		public float moveY;

		public Signal left;
		public Signal right;
		public Signal up;
		public Signal down;
		public bool active = true;
		public string id = "swipeId";

		private void Update(){

			if(!active)
				return;

			if(area != null){
				if(!RectTransformUtility.RectangleContainsScreenPoint(area, Input.mousePosition, null)){
					return;
				}
			}

			if(Input.GetMouseButtonDown(0)){
				startPos = Input.mousePosition;
			}

			if(Input.GetMouseButtonUp(0)){
				var swipe = (Vector2) Input.mousePosition - startPos;

				if(swipe.x >= moveX && right != null)
					right.Dispatch(id);

				if(swipe.x <= -moveX && left != null)
					left.Dispatch(id);

				if(swipe.y >= moveY && up != null)
					up.Dispatch(id);

				if(swipe.y <= -moveY && down != null)
					down.Dispatch(id);

			}
		}
	}
}
