using UnityEngine;

namespace Nonatomic.Utomic {

	public class TouchInput : MonoBehaviour {

		public Signal touchBegan;
		public Signal touchMoved;
		public Signal touchEnded;
		public Signal touchStationary;

		private void Update(){

			foreach (Touch touch in Input.touches) {
				switch(touch.phase){
					case TouchPhase.Began:
						if(touchBegan != null)
							touchBegan.Dispatch();
					break;
					case TouchPhase.Ended:
						if(touchEnded != null)
							touchEnded.Dispatch();
					break;
					case TouchPhase.Moved:
						if(touchMoved != null)
							touchMoved.Dispatch();
					break;
					case TouchPhase.Stationary:
						if(touchMoved != null)
							touchMoved.Dispatch();
					break;
				}
			}
		}
	}
}
