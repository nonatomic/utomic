using UnityEngine;
using UnityEngine.UI;
using Nonatomic.Utomic;
using UnityEngine.EventSystems;

namespace Nonatomic.Utomic {


	public class DragComponent : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

		public Signal onDrag;

		public void OnBeginDrag(PointerEventData data)
		{
			// this.Log("OnBeginDrag", "yellow");
			SetDraggedPosition(data, true);
		}

		public void OnDrag(PointerEventData data)
		{
			// this.Log("OnDrag", "yellow");
			SetDraggedPosition(data, true);
		}

		private void SetDraggedPosition(PointerEventData data, bool isDragging)
		{
			// this.Log("POS:" + data.delta + ", " + data.dragging + ", " + end + ", " + data.pointerDrag.name + ", " + data.scrollDelta, "yellow");
			if(onDrag != null){
				onDrag.Dispatch(data.delta, data.pointerDrag, isDragging);
			}
		}

		public void OnEndDrag(PointerEventData data)
		{
			// this.Log("OnEndDrag", "yellow");
			SetDraggedPosition(data, false);
		}
	}
}
