using UnityEngine;
using UnityEngine.UI;
using Nonatomic.Utomic;
using UnityEngine.EventSystems;
using System.Collections;

namespace Nonatomic.Utomic {

	public class ScreenshotComponent : MonoBehaviour {

		public void TakeScreenshot(Rect rect, Signal complete){
			StartCoroutine(Capture(rect, complete));
		}

		private IEnumerator Capture(Rect rect, Signal complete){

			yield return new WaitForEndOfFrame();

			var tex = new Texture2D((int)rect.width, (int)rect.height, TextureFormat.RGB24, false);
			tex.ReadPixels(rect, 0, 0);
			tex.Apply();

			complete.Dispatch(tex);
		}
	}
}
