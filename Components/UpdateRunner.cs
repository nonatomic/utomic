using UnityEngine;
using System.Collections.Generic;
using System;

namespace Nonatomic.Utomic {

	public class UpdateRunner : MonoBehaviour {

		private List<Action> methods = new List<Action>();

		public void Add(Action method){
			methods.Add(method);
		}

		public void Remove(Action method){
			if(methods.Contains(method)){
				methods.Remove(method);
			}
		}

		public void Update(){
			methods.ForEach(item => item.Invoke());
		}
	}
}
