//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using System;
using UnityEngine;
using System.Collections.Generic;

namespace Nonatomic.Utomic {

	public static class RectTransformExtension {

		public static void SetDepth(this RectTransform obj, int depth) {

			var depthComponent = obj.gameObject.GetComponent<DepthComponent>();
			if(depthComponent == null){
				depthComponent = obj.gameObject.AddComponent<DepthComponent>();
				obj.EntityList().AddComponent(depthComponent);
			}
			depthComponent.value = depth;
		}

		public static int GetDepth(this RectTransform obj) {

			var depthComponent = obj.gameObject.GetComponent<DepthComponent>();
			return depthComponent == null ? -1 : depthComponent.value;
		}

		public static float GetMinX(this RectTransform transform) {
			return transform.anchoredPosition.x - transform.pivot.x * transform.GetWidth();
		}

		public static float GetMaxX(this RectTransform transform) {
			return transform.GetMinX() + transform.GetWidth();
		}

		public static float GetMinY(this RectTransform transform) {
			return transform.anchoredPosition.y - transform.pivot.y * transform.GetHeight();
		}

		public static float GetMaxY(this RectTransform transform) {
			return transform.GetMinY() + transform.GetHeight();
		}

		public static RectTransform SetAnchorCenter(this RectTransform transform) {
			transform.anchorMin = new Vector2(0.5f, 0.5f);
			transform.anchorMax = new Vector2(0.5f, 0.5f);
			return transform;
		}

		public static RectTransform SetAnchorMinMax(this RectTransform transform, Vector2 min, Vector2 max) {
			transform.anchorMin = min;
			transform.anchorMax = max;
			return transform;
		}

		public static RectTransform SetAnchorMinMax(this RectTransform transform, float min, float max) {
			transform.anchorMin = new Vector2(min, min);
			transform.anchorMax = new Vector2(max, max);
			return transform;
		}

		public static RectTransform SetAnchorMinMax(this RectTransform transform, float minX, float minY, float maxX, float maxY) {
			transform.anchorMin = new Vector2(minX, minY);
			transform.anchorMax = new Vector2(maxX, maxY);
			return transform;
		}

		public static RectTransform SetAnchorMinMax(this RectTransform transform, List<float> anchor) {

			if(anchor.Count == 2){
				SetAnchorMinMax(transform, anchor[0], anchor[1]);
			}
			else if(anchor.Count == 4){
				SetAnchorMinMax(transform, anchor[0], anchor[1], anchor[2], anchor[3]);
			}

			return transform;
		}

		public static RectTransform SetOffsetZero(this RectTransform transform) {
			transform.offsetMin = Vector2.zero;
			transform.offsetMax = Vector2.zero;
			return transform;
		}

		public static RectTransform SetOffsetMinMax(this RectTransform transform, Vector2 min, Vector2 max) {
			transform.offsetMin = min;
			transform.offsetMax = max;
			return transform;
		}

		public static RectTransform SetOffsetMinMax(this RectTransform transform, float min, float max) {
			transform.offsetMin = new Vector2(min, min);
			transform.offsetMax = new Vector2(max, max);
			return transform;
		}

		public static RectTransform SetOffsetMinMax(this RectTransform transform, float minX, float minY, float maxX, float maxY) {
			transform.offsetMin = new Vector2(minX, minY);
			transform.offsetMax = new Vector2(maxX, maxY);
			return transform;
		}

		public static RectTransform SetSizeZero(this RectTransform transform) {
			transform.sizeDelta = Vector2.zero;
			return transform;
		}

		public static RectTransform SetSize(this RectTransform transform, float width, float height) {
			transform.sizeDelta = new Vector2(width, height);
			return transform;
		}

		public static RectTransform SetSize(this RectTransform transform, int width, int height) {
			transform.sizeDelta = new Vector2(width, height);
			return transform;
		}

		public static RectTransform SetSize(this RectTransform transform, Vector2 size) {
			transform.sizeDelta = size;
			return transform;
		}

		public static RectTransform SetWidth(this RectTransform transform, float width) {
			transform.sizeDelta = new Vector2(width, transform.sizeDelta.y);
			return transform;
		}

		public static RectTransform SetHeight(this RectTransform transform, float height) {
			transform.sizeDelta = new Vector2(transform.sizeDelta.x, height);
			return transform;
		}

		public static RectTransform SetPositionZero(this RectTransform transform) {
			transform.anchoredPosition = Vector2.zero;
			return transform;
		}

		public static RectTransform SetPosition(this RectTransform transform, float x, float y) {
			transform.anchoredPosition = new Vector2(x, y);
			return transform;
		}

		public static RectTransform SetPosition(this RectTransform transform, int x, int y) {
			transform.anchoredPosition = new Vector2(x, y);
			return transform;
		}

		public static RectTransform SetDefaultScale(this RectTransform transform) {
			transform.localScale = new Vector3(1, 1, 1);
			return transform;
		}

		public static RectTransform SetScaleZero(this RectTransform transform) {
			transform.localScale = Vector2.zero;
			return transform;
		}

		public static RectTransform SetPivotAndAnchors(this RectTransform transform, Vector2 vec) {
			transform.pivot = vec;
			transform.anchorMin = vec;
			transform.anchorMax = vec;
			return transform;
		}

		public static RectTransform SetPivotAndAnchors(this RectTransform transform, float x, float y) {
			var vec = new Vector2(x,y);
			transform.pivot = vec;
			transform.anchorMin = vec;
			transform.anchorMax = vec;
			return transform;
		}

		public static RectTransform SetPosition(this RectTransform transform, Vector2 position) {
			transform.localPosition = new Vector3(position.x, position.y, transform.localPosition.z);
			return transform;
		}

		public static RectTransform SetPivot(this RectTransform transform, Vector2 pivot) {
			transform.pivot = pivot;
			return transform;
		}

		public static RectTransform SetPivot(this RectTransform transform, float x, float y) {
			transform.pivot = new Vector2(x,y);
			return transform;
		}

		public static Vector2 GetSize(this RectTransform transform) {
			return transform.rect.size;
		}

		public static float GetWidth(this RectTransform transform) {
			return transform.rect.width;
		}

		public static float GetHeight(this RectTransform transform) {
			return transform.rect.height;
		}

		public static RectTransform SetLeftBottomPosition(this RectTransform transform, Vector2 position) {
			transform.localPosition = new Vector3(position.x + (transform.pivot.x * transform.rect.width), position.y + (transform.pivot.y * transform.rect.height), transform.localPosition.z);
			return transform;
		}

		public static RectTransform SetLeftTopPosition(this RectTransform transform, Vector2 position) {
			transform.localPosition = new Vector3(position.x + (transform.pivot.x * transform.rect.width), position.y - ((1f - transform.pivot.y) * transform.rect.height), transform.localPosition.z);
			return transform;
		}

		public static RectTransform SetRightBottomPosition(this RectTransform transform, Vector2 position) {
			transform.localPosition = new Vector3(position.x - ((1f - transform.pivot.x) * transform.rect.width), position.y + (transform.pivot.y * transform.rect.height), transform.localPosition.z);
			return transform;
		}

		public static RectTransform SetRightTopPosition(this RectTransform transform, Vector2 position) {
			transform.localPosition = new Vector3(position.x - ((1f - transform.pivot.x) * transform.rect.width), position.y - ((1f - transform.pivot.y) * transform.rect.height), transform.localPosition.z);
			return transform;
		}
	}
}
