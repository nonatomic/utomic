//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using System;
using UnityEngine;
using System.Collections.Generic;

namespace Nonatomic.Utomic {

	public static class GameObjectExtension {

		public static RectTransform RectTransform(this GameObject go) {
			return (RectTransform) go.transform;
		}

		public static GameObject PoolAllChildren(this GameObject go) {
			foreach(Transform child in go.transform){
				go.ReturnToPool();
			}
			return go;
		}

		public static GameObject SetParent(this GameObject go, GameObject parent) {
			go.transform.SetParent(parent.transform, false);
			return go;
		}

		public static GameObject SetParent(this GameObject go, Transform parent) {
			go.transform.SetParent(parent, false);
			return go;
		}

		public static GameObject SetPosition(this GameObject go, Vector3 position) {
			go.transform.position = position;
			return go;
		}

		public static GameObject SetPosition(this GameObject go, float x, float y, float z) {
			go.transform.position = new Vector3(x,y,z);
			return go;
		}

		public static GameObject SetPosition(this GameObject go, int x, int y, int z) {
			go.transform.position = new Vector3(x,y,z);
			return go;
		}

		public static GameObject SetPositionX(this GameObject go, int x) {
			go.transform.position = new Vector3(x, go.transform.position.y, go.transform.position.z);
			return go;
		}

		public static GameObject SetPositionX(this GameObject go, float x) {
			go.transform.position = new Vector3(x, go.transform.position.y, go.transform.position.z);
			return go;
		}

		public static GameObject SetPositionY(this GameObject go, int y) {
			go.transform.position = new Vector3(go.transform.position.x, y, go.transform.position.z);
			return go;
		}

		public static GameObject SetPositionY(this GameObject go, float y) {
			go.transform.position = new Vector3(go.transform.position.x, y, go.transform.position.z);
			return go;
		}

		public static GameObject SetPositionZ(this GameObject go, int z) {
			go.transform.position = new Vector3(go.transform.position.x, go.transform.position.y, z);
			return go;
		}

		public static GameObject SetPositionZ(this GameObject go, float z) {
			go.transform.position = new Vector3(go.transform.position.x, go.transform.position.y, z);
			return go;
		}

	}
}
