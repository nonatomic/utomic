//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using System;
using System.Collections.Generic;

namespace Nonatomic.Utomic {

	public static class DictionaryExtension {

		/*
		* Allows incrementation of a dictionary int value
		*/
		public static void Increment<T>(this Dictionary<T, int> dictionary, T key) {
			int count;
			dictionary.TryGetValue(key, out count);
			dictionary[key] = count + 1;
		}
	}
}
