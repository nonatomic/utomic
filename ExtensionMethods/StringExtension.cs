//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using System;
using UnityEngine;
using System.Linq;
using System.Collections.Generic;

namespace Nonatomic.Utomic {

	public static class StringExtension {
/**
		*
		*/
		public static string Shuffle(this string str) {
			var num = new System.Random();
			str.Log("+++++ SHUFFLE +++++", "cyan");
			str.Log("A. STR SHUFFLE:" + num + ", " + str, "cyan");
			var newstr = new string(str.ToCharArray().OrderBy(s => (num.Next(2) % 2) == 0).ToArray());
			str.Log("B. STR SHUFFLE:" + num +", " + newstr, "cyan");
			return newstr;
		}

		public static string ToTitleCase(this string str) {
			str = str.ToLower();
			var newString = String.Empty;
			var words = str.Split(' ');
			var count = 0;

			foreach(string word in words){
				var w = word;

				if(w.Length > 1){

					if(w != "the" || w != "of" || w != "and" || w != "to"){
						var firstChar = w[0].ToString();
						w = firstChar.ToUpper() + w.Substring(1);
					}
				}

				if(count > 0){
					w = " " + w;
				}

				newString += w;
				count++;
			}

			return newString;
		}

		public static List<string> WordWrap(this string str, int charLength) {

			string[] words = str.Split(' ');

			List<string> parts = new List<string>();
			string part = string.Empty;

			foreach (var word in words){

				if (string.IsNullOrEmpty(part) && part.Length + word.Length <= charLength){
					part += word;
				}

				//we need to +1 for the addition of the space
				else if(!string.IsNullOrEmpty(part) && part.Length + word.Length + 1 <= charLength){
					part += " " + word;
				}

				else if(!string.IsNullOrEmpty(part)){
					parts.Add(part);
					part = word;
				}
			}

			parts.Add(part);
			return parts;
		}

		public static string RemoveFirst(this string str, string search, string replace){
			int pos = str.IndexOf(search);

			if(pos < 0){
				return str;
			}
			else{
				return str.Substring(0, pos) + replace + str.Substring(pos + search.Length);
			}
		}

		public static int Distance(this string a, string b){
						
			if (string.IsNullOrEmpty(a) || string.IsNullOrEmpty(b))
				return 0;

			int lengthA = a.Length;
			int lengthB = b.Length;

			var distances = new int[lengthA + 1, lengthB + 1];
			for (int i = 0;  i <= lengthA;  distances[i, 0] = i++);
			for (int j = 0;  j <= lengthB;  distances[0, j] = j++);

			for (int i = 1;  i <= lengthA;  i++) {
				for (int j = 1;  j <= lengthB;  j++) {
					int  cost = b[j - 1] == a[i - 1] ? 0 : 1;
					distances[i, j] = Mathf.Min
					(
					Mathf.Min(distances[i - 1, j] + 1, distances[i, j - 1] + 1),
					distances[i - 1, j - 1] + cost
					);
				}
			}

			return distances[lengthA, lengthB];
		}
	}
}
