//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using System;
using UnityEngine;

namespace Nonatomic.Utomic {

	public static class InjectorExtension {

		/**
		* Bind Type T to instance U
		* Call to Inject T returns Singleton Instance U
		*/
		public static void Bind<T, U>(this object obj) {
			Injector.Instance.Bind<T, U>();
		}

		/**
		* Bind Type T to instance T
		* Call to Inject T returns Singleton Instance T
		*/
		public static void Bind<T>(this object obj, T instance) {
			Injector.Instance.Bind<T>(instance);
		}

		/**
		* Bind Type T to Type T
		* Call to Inject T returns Singleton Instance T
		*/
		public static void Bind<T>(this object obj) {
			Injector.Instance.Bind<T>();
		}

		/**
		* Bind Type T to Type T
		* Call to Inject T returns Singleton Instance T
		* Removes existing instance of T
		*/
		public static void Rebind<T>(this object obj) {
			Injector.Instance.Rebind<T>();
		}

		/**
		* Bind Type T to Type U
		* Call to Inject T returns Singleton Instance U
		* Removes existing instance of U
		*/
		public static void Rebind<T, U>(this object obj) {
			Injector.Instance.Rebind<T, U>();
		}

		/**
		*	Provides a singleton instance of T
		*/
		public static T Inject<T>(this object obj) {
			return Injector.Instance.Inject<T>();
		}

		/**
		*	Provides a singleton instance of typeName
		*/
		public static object Inject(this object obj, string typeName) {
			return Injector.Instance.Inject(typeName);
		}

		/**
		* Is T bound in the Injector
		*/
		public static bool HasBinding<T>(this object obj){
			return Injector.Instance.HasBinding<T>();
		}
	}
}
