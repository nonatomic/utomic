//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace Nonatomic.Utomic {

	public static class ListExtension {

		public static T PopAt<T>(this IList<T> list, int index) {
			T r = list[index];
			list.RemoveAt(index);
			return r;
		}

		public static IList<T> Shuffle<T>(this IList<T> list){
			var rnd = new System.Random();
			list.Log("A. SHUFFLE:" + rnd + ", " + list.ToString());

			int n = list.Count;
			while (n > 1) {
				n--;
				int k = rnd.Next(n + 1);
				T value = list[k];
				list[k] = list[n];
				list[n] = value;
			}

			list.Log("B. SHUFFLE:" + rnd + ", " + list.ToString());
			return list;
		}
	}
}
