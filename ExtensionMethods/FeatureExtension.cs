//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using System;
using UnityEngine;
using System.Collections.Generic;

namespace Nonatomic.Utomic {

	public static class FeatureExtension {

		private static List<IFeature> features = new List<IFeature>();

		/*
		*
		*/
		public static void AddFeature<T>(this object obj) where T:IFeature, new() {
			T feature = new T();
			feature.Log("Add Feature: [" + features.Count + "] ", feature.GetType().Name, "green", "yellow");
			feature.Wire();
			features.Add(feature);
		}

		/**
		* Build early infrastructure like layers, cameras etc
		*/
		public static void StructureFeatures(this object obj){

			foreach(IFeature feature in features){
				feature.Structure();
			}
		}

		/**
		* Setup menus
		*/
		public static void SetupFeatures(this object obj){

			foreach(IFeature feature in features){
				feature.Setup();
			}
		}

		/**
		* Run game logic, show first scene/menu
		*/
		public static void RunFeatures(this object obj){

			foreach(IFeature feature in features){
				feature.Run();
			}
		}
	}
}
