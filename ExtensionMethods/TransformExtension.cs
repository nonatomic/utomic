//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using System;
using UnityEngine;
using System.Collections.Generic;

namespace Nonatomic.Utomic {

	public static class TransformExtension {

		public static Transform SetPosition(this Transform transform, Vector3 position) {
			transform.position = position;
			return transform;
		}

		public static Transform SetPosition(this Transform transform, float x, float y, float z) {
			transform.position = new Vector3(x,y,z);
			return transform;
		}

		public static Transform SetPosition(this Transform transform, int x, int y, int z) {
			transform.position = new Vector3(x,y,z);
			return transform;
		}

		public static Transform SetPositionX(this Transform transform, int x) {
			transform.position = new Vector3(x, transform.position.y, transform.position.z);
			return transform;
		}

		public static Transform SetPositionX(this Transform transform, float x) {
			transform.position = new Vector3(x, transform.position.y, transform.position.z);
			return transform;
		}

		public static Transform SetPositionY(this Transform transform, int y) {
			transform.position = new Vector3(transform.position.x, y, transform.position.z);
			return transform;
		}

		public static Transform SetPositionY(this Transform transform, float y) {
			transform.position = new Vector3(transform.position.x, y, transform.position.z);
			return transform;
		}

		public static Transform SetPositionZ(this Transform transform, int z) {
			transform.position = new Vector3(transform.position.x, transform.position.y, z);
			return transform;
		}

		public static Transform SetPositionZ(this Transform transform, float z) {
			transform.position = new Vector3(transform.position.x, transform.position.y, z);
			return transform;
		}

	}
}
