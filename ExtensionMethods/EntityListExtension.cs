//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using System;
using UnityEngine;

namespace Nonatomic.Utomic {

	public static class EntityListExtension {

		private static IEntityList entityList;

		/*
		*
		*/
		public static IEntityList EntityList(this object obj) {

			if(!obj.HasBinding<IEntityList>()){
				obj.Bind<IEntityList, EntityList>();
			}

			return entityList = entityList ?? obj.Inject<IEntityList>();
		}

		/**
		* Add a component to an entity and entity list
		*/
		public static T AddEntityComponent<T>(this GameObject obj) where T : Component {

			var component = obj.AddComponent<T>();
			obj.EntityList().AddComponent(component);
			return component;
		}

		/**
		* Add a component to an entity and entity list
		*/
		public static void RemoveEntityComponent<T>(this GameObject obj) where T : Component {

			var component = obj.GetComponent<T>();
			obj.EntityList().RemoveComponent(component, true);
		}
	}
}
