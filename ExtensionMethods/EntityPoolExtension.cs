//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using System;
using UnityEngine;

namespace Nonatomic.Utomic {

	public static class EntityPoolExtension {

		private static IEntityPool entityPool;

		/**
		*
		*/
		public static IEntityPool EntityPool(this object obj) {

			if(!obj.HasBinding<IEntityPool>()){
				obj.Bind<IEntityPool, EntityPool>();
			}

			return entityPool = entityPool ?? obj.Inject<IEntityPool>();
		}

		/**
		* Return an entity to the pool
		*/
		public static void ReturnToPool(this GameObject obj){

			obj.EntityPool().Push(obj);
		}
	}
}
