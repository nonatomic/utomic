//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using System;
using UnityEngine;

namespace Nonatomic.Utomic {

	public static class CommandExtension {

		private static ICommandMap commandMap;

		/**
		*
		*/
		public static ICommandMap CommandMap(this object obj) {

			if(!obj.HasBinding<ICommandMap>()){
				obj.Bind<ICommandMap, CommandMap>();
			}

			return commandMap = commandMap ?? obj.Inject<ICommandMap>();
		}
	}
}
