//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using System;
using UnityEngine;
using System.Collections.Generic;

namespace Nonatomic.Utomic {

	public static class LogExtension {

		private static List<string> colorFilter = new List<string>();
		private static List<string> keyWordFilter = new List<string>();
		private static bool enabled = true;

		public static void LogEnabled(this object obj, bool enable){
			enabled = enable;
		}

		public static bool IsLogEnabled(this object obj){
			return enabled;
		}

		#region Filters
		public static void LogColorFilter(this object obj, string color){

			colorFilter = new List<string>(){color};
		}

		public static void AddLogColorFilter(this object obj, string color){

			colorFilter.Add(color);
		}

		public static void LogColorFilter(this object obj, string[] colors){

			colorFilter = new List<string>();
			foreach(string color in colors)
    			colorFilter.Add(color);
		}

		public static void LogColorFilter(this object obj, List<string> colors){

			colorFilter = colors;
		}

		public static void LogKeyFilter(this object obj, string keyword){

			keyWordFilter = new List<string>(){keyword};
		}

		public static void AddLogKeyFilter(this object obj, string keyword){

			keyWordFilter.Add(keyword);
		}

		public static void LogKeyFilter(this object obj, string[] keywords){

			keyWordFilter = new List<string>();
			foreach(string word in keywords)
    			keyWordFilter.Add(word);
		}

		public static void LogKeyFilter(this object obj, List<string> keywords){

			keyWordFilter = keywords;
		}

		private static bool KeyFilterGuard(object msg){

			if(colorFilter.Count == 0 && keyWordFilter.Count == 0)
				return false;

			foreach(string key in keyWordFilter){
				if(msg != null){
					// Debug.Log(msg.ToString().Contains(key) + " = " + msg.ToString());
				}

				if(msg != null && msg.ToString().Contains(key)){
					return false;
				}
			}

			return true;
		}

		private static bool ColorFilterGuard(string color){

			if(colorFilter.Count == 0 && keyWordFilter.Count == 0)
				return false;

			if(colorFilter.Contains(color)){
				return false;
			}

			return true;
		}
		#endregion

		#region logging
		public static void Log(this object obj, object msg){

			if(!enabled)
				return;

			if(KeyFilterGuard(msg))
				return;

			if(Application.isEditor){
				MonoBehaviour.print(TimeString() + msg + "\n");
			}
			else{
				MonoBehaviour.print(TimeString() + msg );
			}
		}

		public static void Log(this object obj, object msg, Color32 color, int size = 11){
			Log(obj, msg, ColorToHex(color), size);
		}

		public static void Log(this object obj, object msg, string color, int size = 11){

			if(!enabled)
				return;

			if(KeyFilterGuard(msg) && ColorFilterGuard(color))
				return;

			if(Application.isEditor){
				MonoBehaviour.print(TimeString() + "<size=" + size + "><color=" + color + ">" + msg + "</color></size>\n");
			}
			else{
				MonoBehaviour.print(TimeString() + msg );
			}
		}

		public static void Log(this object obj, object tag, object msg, Color32 tagColor, Color32 msgColor, int size = 11){
			Log(obj, tag, msg, ColorToHex(tagColor), ColorToHex(msgColor), size );
		}

		public static void Log(this object obj, object tag, object msg, string tagColor, string msgColor, int size = 11){

			if(!enabled)
				return;

			if(KeyFilterGuard(msg) && KeyFilterGuard(tag) && ColorFilterGuard(tagColor) && ColorFilterGuard(msgColor))
				return;

			if(Application.isEditor){
				string tagStr = "<b><color=" + tagColor + ">" + tag + "</color>:</b> ";
				string msgStr = "<color=" + msgColor + ">" + msg + "</color>\n";
				MonoBehaviour.print(TimeString() + "<size=" + size + ">" + tagStr + msgStr + "</size>");
			}
			else{
				MonoBehaviour.print(TimeString() + tag + ": " + msg );
			}
		}

		#pragma warning disable 0219 //disable warning for not using color
		public static void LogWarning(this object obj, object msg, int size = 11){

			string color = "yellow";

			if(Application.isEditor){
				MonoBehaviour.print(TimeString() + "<size=" + size + "><color=" + color + ">Warning:" + msg + "</color></size>\n");
			}
			else{
				MonoBehaviour.print(TimeString() + "Warning:" + msg);
			}
		}

		public static void LogError(this object obj, object msg, int size = 11){

			string color = "red";

			if(Application.isEditor){
				MonoBehaviour.print(TimeString() + "<size=" + size + "><color=" + color + ">Error:" + msg + "</color></size>\n");
			}
			else{
				MonoBehaviour.print(TimeString() + "Error:" + msg);
			}
		}
		#endregion

		#region helper
		private static string TimeString(){

			if(Application.isEditor){
				return "<color=white>" + System.DateTime.Now.ToString("mm:ss.ffff") + "</color> :: ";
			}
			else{
				return System.DateTime.Now.ToString("mm:ss.ffff") + " :: ";
			}
		}

		private static string ColorToHex(Color32 color){
			return "#" + color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
		}
		#endregion
	}
}
