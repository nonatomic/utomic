//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using System;
using System.Collections.Generic;

namespace Nonatomic.Utomic {

	// Utility class that simplifies cration of tuples by using
	// method calls instead of constructor calls
	public static class Tuple {

		// Creates a new tuple value with the specified elements. The method
		// can be used without specifying the generic parameters, because C#
		// compiler can usually infer the actual types.
		public static Tuple<T1, T2> Create<T1, T2>(T1 item1, T2 second) {
			return new Tuple<T1, T2>(item1, second);
		}

		// Creates a new tuple value with the specified elements. The method
		// can be used without specifying the generic parameters, because C#
		// compiler can usually infer the actual types.
		public static Tuple<T1, T2, T3> Create<T1, T2, T3>(T1 item1, T2 second, T3 third) {
			return new Tuple<T1, T2, T3>(item1, second, third);
		}

		// Creates a new tuple value with the specified elements. The method
		// can be used without specifying the generic parameters, because C#
		// compiler can usually infer the actual types.
		public static Tuple<T1, T2, T3, T4> Create<T1, T2, T3, T4>(T1 item1, T2 second, T3 third, T4 fourth) {
			return new Tuple<T1, T2, T3, T4>(item1, second, third, fourth);
		}

		// Extension method that provides a concise utility for unpacking
		// tuple components into specific out parameters.
		public static void Unpack<T1, T2>(this Tuple<T1, T2> tuple, out T1 ref1, out T2 ref2) {
			ref1 = tuple.Item1;
			ref2 = tuple.Item2;
		}

		// Extension method that provides a concise utility for unpacking
		// tuple components into specific out parameters.
		public static void Unpack<T1, T2, T3>(this Tuple<T1, T2, T3> tuple, out T1 ref1, out T2 ref2, T3 ref3) {
			ref1 = tuple.Item1;
			ref2 = tuple.Item2;
			ref3 = tuple.Item3;
		}


		// Extension method that provides a concise utility for unpacking
		// tuple components into specific out parameters.
		public static void Unpack<T1, T2, T3, T4>(this Tuple<T1, T2, T3, T4> tuple, out T1 ref1, out T2 ref2, T3 ref3, T4 ref4) {
			ref1 = tuple.Item1;
			ref2 = tuple.Item2;
			ref3 = tuple.Item3;
			ref4 = tuple.Item4;
		}
	}
}
