//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;

namespace Nonatomic.Utomic {

	public interface ICommandMap {

		Signal Bind<T, U>() where T:Signal where U:ICommand;
		Signal BindOnce<T, U>() where T:Signal where U:ICommand;
		Signal Rebind<T, U>() where T:Signal where U:ICommand;
		Signal RebindOnce<T, U>() where T:Signal where U:ICommand;
	}
}
