//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using System;
using System.Collections.Generic;

namespace Nonatomic.Utomic {

	// Represents a functional tuple that can be used to store
	// two values of different types inside one object.
	public sealed class Tuple<T1, T2, T3> {
		private readonly T1 item1;
		private readonly T2 item2;
		private readonly T3 item3;

		// Retyurns the first element of the tuple
		public T1 Item1 {
			get { return item1; }
		}

		// Returns the second element of the tuple
		public T2 Item2 {
			get { return item2; }
		}

		// Returns the second element of the tuple
		public T3 Item3 {
			get { return item3; }
		}

		// Create a new tuple value
		public Tuple(T1 item1, T2 item2, T3 item3) {
			this.item1 = item1;
			this.item2 = item2;
			this.item3 = item3;
		}

		public override int GetHashCode() {
			int hash = 17;
			hash = hash * 23 + (item1 == null ? 0 : item1.GetHashCode());
			hash = hash * 23 + (item2 == null ? 0 : item2.GetHashCode());
			hash = hash * 23 + (item3 == null ? 0 : item3.GetHashCode());
			return hash;
		}

		// public override bool Equals(object o) {
		// 	if (!(o is Tuple<T1, T2, T3>)) {
		// 		return false;
		// 	}

		// 	var other = (Tuple<T1, T2, T3>)o;

		// 	return this == other;
		// }

		// public static bool operator==(Tuple<T1, T2, T3> a, Tuple<T1, T2, T3> b) {
		// 	if (object.ReferenceEquals(a, null)) {
		// 		return object.ReferenceEquals(b, null);
		// 	}
		// 	if (a.item1 == null && b.item1 != null) return false;
		// 	if (a.item2 == null && b.item2 != null) return false;
		// 	if (a.item3 == null && b.item3 != null) return false;
		// 	return
		// 		a.item1.Equals(b.item1) &&
		// 		a.item2.Equals(b.item2) &&
		// 		a.item3.Equals(b.item3);
		// }

		// public static bool operator!=(Tuple<T1, T2, T3> a, Tuple<T1, T2, T3> b) {
		// 	return !(a == b);
		// }

		public void Unpack(Action<T1, T2, T3> unpackerDelegate) {
			unpackerDelegate(Item1, Item2, Item3);
		}
	}
}
