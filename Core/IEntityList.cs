//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using System.Collections.Generic;
using UnityEngine;
using System;

namespace Nonatomic.Utomic {

	public interface IEntityList {

		int EntityCount {get;}
		int ComponentCount();
		int ComponentCount<T>() where T:Component;
		int ComponentTypeCount { get; }
		void AddEntity(GameObject entity);
		void AddComponent(Component component);

		GameObject GetEntity(Type type);
		GameObject GetEntity<T>() where T:Component;
		GameObject GetEntity<T>(Predicate<GameObject> search) where T:Component;

		List<GameObject> GetEntities(params Type[] componentTypes);
		List<GameObject> GetEntities<T>(Predicate<GameObject> search) where T:Component;
		List<GameObject> GetEntities<T>() where T:Component;
		List<GameObject> GetEntities<T,U>() where T:Component where U:Component;
		List<GameObject> GetEntities<T,U>(Predicate<GameObject> search) where T:Component where U:Component;
		List<GameObject> GetEntities<T,U,V>() where T:Component where U:Component where V:Component;
		List<GameObject> GetEntities<T,U,V>(Predicate<GameObject> search) where T:Component where U:Component where V:Component;
		List<GameObject> GetEntities<T,U,V,W>() where T:Component where U:Component where V:Component where W:Component;
		List<GameObject> GetEntities<T,U,V,W>(Predicate<GameObject> search) where T:Component where U:Component where V:Component where W:Component;

		GameObject GetFirstEntity(params Type[] componentTypes);
		GameObject GetFirstEntity<T,U,V,W>() where T:Component where U:Component where V:Component where W:Component;
		GameObject GetFirstEntity<T,U,V>() where T:Component where U:Component where V:Component;
		GameObject GetFirstEntity<T,U>() where T:Component where U:Component;
		GameObject GetFirstEntity<T>(bool destroy = true) where T:Component;

		GameObject GetLastEntity(bool destroy = true, params Type[] componentTypes);
		GameObject GetLastEntity<T,U,V,W>(bool destroy = true) where T:Component where U:Component where V:Component where W:Component;
		GameObject GetLastEntity<T,U,V>(bool destroy = true) where T:Component where U:Component where V:Component;
		GameObject GetLastEntity<T,U>(bool destroy = true) where T:Component where U:Component;
		GameObject GetLastEntity<T>(bool destroy = true) where T:Component;

		List<T> GetComponents<T>() where T:Component;
		List<T> GetComponents<T>(Predicate<T> search) where T:Component;

		T GetComponent<T>() where T:Component;
		T GetComponent<T>(Predicate<T> search) where T:Component;
		Tuple<T,U> GetComponent<T,U>() where T:Component where U:Component;
		Tuple<T,U,V> GetComponent<T,U,V>() where T:Component where U:Component where V:Component;

		Tuple<List<T>,List<U>> GetComponents<T,U>() where T:Component where U:Component;
		Tuple<List<T>,List<U>,List<V>> GetComponents<T,U,V>() where T:Component where U:Component where V:Component;

		bool HasComponents(GameObject entity, params Type[] componentTypes);
		bool HasComponents<T,U,V,W>(GameObject entity) where T:Component where U:Component where V:Component where W:Component;
		bool HasComponents<T,U,V>(GameObject entity) where T:Component where U:Component where V:Component;
		bool HasComponents<T,U>(GameObject entity) where T:Component where U:Component;

		void RemoveEntity(GameObject entity, bool destroy = true);
		void RemoveEntities(bool destroy = true, params Type[] componentTypes);
		void RemoveEntities<T,U,V,W>(bool destroy = true) where T:Component where U:Component where V:Component where W:Component;
		void RemoveEntities<T,U,V>(bool destroy = true) where T:Component where U:Component where V:Component;
		void RemoveEntities<T,U>(bool destroy = true) where T:Component where U:Component;
		void RemoveEntities<T>(bool destroy = true) where T:Component;

		void RemoveFirstEntity(bool destroy = true, params Type[] componentTypes);
		void RemoveFirstEntity<T,U,V,W>(bool destroy = true) where T:Component where U:Component where V:Component where W:Component;
		void RemoveFirstEntity<T,U,V>(bool destroy = true) where T:Component where U:Component where V:Component;
		void RemoveFirstEntity<T,U>(bool destroy = true) where T:Component where U:Component;
		void RemoveFirstEntity<T>(bool destroy = true) where T:Component;

		void RemoveLastEntity(bool destroy = true, params Type[] componentTypes);
		void RemoveLastEntity<T,U,V,W>(bool destroy = true) where T:Component where U:Component where V:Component where W:Component;
		void RemoveLastEntity<T,U,V>(bool destroy = true) where T:Component where U:Component where V:Component;
		void RemoveLastEntity<T,U>(bool destroy = true) where T:Component where U:Component;
		void RemoveLastEntity<T>(bool destroy = true) where T:Component;

		void RemoveChildEntities(GameObject parent, bool destroy = true);
		void RemoveChildEntities<T>(GameObject parent, bool destroy = true) where T:Component;

		void RemoveComponent(Component component, bool destroy = true);
		void OutputComponentTypes();
		GameObject GetIEntity<T>() where T:class;
		List<GameObject> GetIEntities<T>() where T:class;
		List<T> GetIComponents<T>() where T:class;
		T GetIComponent<T>() where T:class;
	}
}
