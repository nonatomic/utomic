//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Nonatomic.Utomic {

	public class EntityList : IEntityList {

		private Dictionary<Type, List<GameObject>> entityMap = new Dictionary<Type, List<GameObject>>();
		private Dictionary<Type, List<Component>> componentMap = new Dictionary<Type, List<Component>>();
		private List<GameObject> entities = new List<GameObject>();

		/**
		* How many different component types are registered
		*/
		public int ComponentTypeCount {
			get{
				return componentMap.Count;
			}
		}

		/**
		* How many components are registered
		*/
		public int ComponentCount(){
			int count = 0;
			foreach(KeyValuePair<Type, List<Component>> entry in componentMap)
			{
				List<Component> list = entry.Value;
				count += list.Count;
			}
			return count;
		}

		/**
		* How many components of type T are registered in the EntityList
		*/
		public int ComponentCount<T>() where T:Component {
			List<T> components = GetComponents<T>();
			return components.Count;
		}

		public int EntityCount{
			get{
				return entities.Count;
			}
		}

		/**
		*	Add an entity to the list. Maps the entity by its components
		*/
		public void AddEntity(GameObject entity) {
			var components = entity.GetComponents<Component>();
			foreach (Component component in components) {
				if(!entities.Contains(entity)){
					entities.Add(entity);
				}
				AddComponent(component);
			}
		}

		/**
		* Remove & destroy the paramater Entity from the EntityList
		*/
		public void RemoveEntity(GameObject entity, bool destroy = true) {

			if(entity == null)
				return;

			var components = entity.GetComponents<Component>();
			foreach (Component component in components) {

				if(entities.Contains(entity)){
					entities.Remove(entity);
				}
				RemoveComponent(component, false);
			}

			if(destroy && entity != null){
				MonoBehaviour.Destroy(entity);
			}
		}

		/**
		* Remove & destroy the children of an Entity
		*/
		public void RemoveChildEntities(GameObject parent, bool destroy = true){

			//first get list of valid entities
			var children = new List<GameObject>();
			foreach(GameObject entity in entities){
				if(parent != null && entity != null && entity.transform.IsChildOf(parent.transform) && entity != parent){
					children.Add(entity);
				}
			}

			//now remove these entites
			foreach(GameObject entity in children){
				RemoveEntity(entity, destroy);
			}
		}

		/**
		* Remove & destroy the children of an Entity with component T
		*/
		public void RemoveChildEntities<T>(GameObject parent, bool destroy = true) where T:Component {

			//first get list of valid entities
			var children = new List<GameObject>();
			foreach(GameObject entity in entities){
				if(parent != null && entity != null && entity.transform.IsChildOf(parent.transform) && entity != parent && entity.GetComponent<T>() != null){
					children.Add(entity);
				}
			}

			//now remove these entites
			foreach(GameObject entity in children){
				RemoveEntity(entity, destroy);
			}
		}

		/**
		* Removes & destroy all entities with a specific component
		*/
		public void RemoveEntities<T>(bool destroy = true) where T:Component {
			List<T> components = GetComponents<T>();
			foreach(T item in components) {
				RemoveEntity(item.gameObject, destroy);
			}
		}

		/**
		* Removes & destroys all entities registered with all component types
		*/
		public void RemoveEntities(bool destroy = true, params Type[] componentTypes) {
			List<GameObject> entities = GetEntities(componentTypes);
			foreach(GameObject entity in entities) {
				RemoveEntity(entity, destroy);
			}
		}

		/**
		* Removes & destroys all entities registered with 2 component types
		*/
		public void RemoveEntities<T,U>(bool destroy = true) where T:Component where U:Component {
			var types = new Type[2]{typeof(T), typeof(U)};
			RemoveEntities(destroy, types);
		}

		/**
		* Removes & destroys all entities registered with 3 component types
		*/
		public void RemoveEntities<T,U,V>(bool destroy = true) where T:Component where U:Component  where V:Component{
			var types = new Type[3]{typeof(T), typeof(U), typeof(V)};
			RemoveEntities(destroy, types);
		}

		/**
		* Removes & destroys all entities registered with 4 component types
		*/
		public void RemoveEntities<T,U,V,W>(bool destroy = true) where T:Component where U:Component where V:Component where W:Component{
			var types = new Type[4]{typeof(T), typeof(U), typeof(V), typeof(W)};
			RemoveEntities(destroy, types);
		}

		/**
		* Removes & destroys the first entity registered with all component types
		*/
		public void RemoveFirstEntity(bool destroy = true, params Type[] componentTypes) {
			List<GameObject> entities = GetEntities(componentTypes);
			if(entities.Count > 0){
				RemoveEntity(entities[0], destroy);
			}
		}

		/**
		* Removes & destroys the first entity registered with 2 component types
		*/
		public void RemoveFirstEntity<T,U>(bool destroy = true) where T:Component where U:Component{
			var types = new Type[2]{typeof(T), typeof(U)};
			RemoveFirstEntity(destroy, types);
		}

		/**
		* Removes & destroys the first entity registered with 3 component types
		*/
		public void RemoveFirstEntity<T,U,V>(bool destroy = true) where T:Component where U:Component where V:Component{
			var types = new Type[3]{typeof(T), typeof(U), typeof(V)};
			RemoveFirstEntity(destroy, types);
		}

		/**
		* Removes & destroys the first entity registered with 4 component types
		*/
		public void RemoveFirstEntity<T,U,V,W>(bool destroy = true) where T:Component where U:Component where V:Component where W:Component{
			var types = new Type[4]{typeof(T), typeof(U), typeof(V), typeof(W)};
			RemoveFirstEntity(destroy, types);
		}

		/**
		*	Remove & destroy the first entity found with T Component
		*/
		public void RemoveFirstEntity<T>(bool destroy = true) where T:Component {
			List<T> components = GetComponents<T>();
			if(components.Count > 0){
				RemoveEntity(components[0].gameObject, destroy);
			}
		}

		/**
		* Returns the first entity registered with all component types
		*/
		public GameObject GetFirstEntity(params Type[] componentTypes) {
			List<GameObject> entities = GetEntities(componentTypes);
			if(entities.Count > 0){
				return entities[0];
			}

			return null;
		}

		/**
		* Returns the first entity registered with 2 component types
		*/
		public GameObject GetFirstEntity<T,U>() where T:Component where U:Component {
			var types = new Type[2]{typeof(T), typeof(U)};
			return GetFirstEntity(types);
		}

		/**
		* Returns the first entity registered with 3 component types
		*/
		public GameObject GetFirstEntity<T,U,V>() where T:Component where U:Component where V:Component {
			var types = new Type[3]{typeof(T), typeof(U), typeof(V)};
			return GetFirstEntity(types);
		}

		/**
		* Returns the first entity registered with 4 component types
		*/
		public GameObject GetFirstEntity<T,U,V,W>() where T:Component where U:Component where V:Component where W:Component {
			var types = new Type[4]{typeof(T), typeof(U), typeof(V), typeof(W)};
			return GetFirstEntity(types);
		}

		/**
		*	Returns the first entity found with T Component
		*/
		public GameObject GetFirstEntity<T>(bool destroy = true) where T:Component {
			List<T> components = GetComponents<T>();
			if(components.Count > 0){
				return components[0].gameObject;
			}

			return null;
		}

		/**
		*	Remove & destroy the last entity found with T Component
		*/
		public void RemoveLastEntity<T>(bool destroy = true) where T:Component {
			List<T> components = GetComponents<T>();
			if(components.Count > 0){
				RemoveEntity(components.Last().gameObject, destroy);
			}
		}

		/**
		*	Return the last entity found with T Component
		*/
		public GameObject GetLastEntity<T>(bool destroy = true) where T:Component {
			List<T> components = GetComponents<T>();
			if(components.Count > 0){
				return components.Last().gameObject;
			}

			return null;
		}

		/**
		* Removes & destroys the last entity registered with all component types
		*/
		public void RemoveLastEntity(bool destroy = true, params Type[] componentTypes) {
			List<GameObject> entities = GetEntities(componentTypes);
			if(entities.Count > 0){
				RemoveEntity(entities.Last(), destroy);
			}
		}

		/**
		* Removes & destroys the last entity registered with 2 component types
		*/
		public void RemoveLastEntity<T,U>(bool destroy = true) where T:Component where U:Component {
			var types = new Type[2]{typeof(T), typeof(U)};
			RemoveLastEntity(destroy, types);
		}

		/**
		* Removes & destroys the last entity registered with 3 component types
		*/
		public void RemoveLastEntity<T,U,V>(bool destroy = true) where T:Component where U:Component where V:Component {
			var types = new Type[3]{typeof(T), typeof(U), typeof(V)};
			RemoveLastEntity(destroy, types);
		}

		/**
		* Removes & destroys the last entity registered with 4 component types
		*/
		public void RemoveLastEntity<T,U,V,W>(bool destroy = true) where T:Component where U:Component where V:Component where W:Component{
			var types = new Type[4]{typeof(T), typeof(U), typeof(V), typeof(W)};
			RemoveLastEntity(destroy, types);
		}

		/**
		* Return the last entity registered with all component types
		*/
		public GameObject GetLastEntity(bool destroy = true, params Type[] componentTypes) {
			List<GameObject> entities = GetEntities(componentTypes);
			if(entities.Count > 0){
				return entities.Last();
			}

			return null;
		}

		/**
		* Return the last entity registered with 2 component types
		*/
		public GameObject GetLastEntity<T,U>(bool destroy = true) where T:Component where U:Component {
			var types = new Type[2]{typeof(T), typeof(U)};
			return GetLastEntity(destroy, types);
		}

		/**
		* Return the last entity registered with 3 component types
		*/
		public GameObject GetLastEntity<T,U,V>(bool destroy = true) where T:Component where U:Component where V:Component {
			var types = new Type[3]{typeof(T), typeof(U), typeof(V)};
			return GetLastEntity(destroy, types);
		}

		/**
		* Return the last entity registered with 4 component types
		*/
		public GameObject GetLastEntity<T,U,V,W>(bool destroy = true) where T:Component where U:Component where V:Component where W:Component {
			var types = new Type[4]{typeof(T), typeof(U), typeof(V), typeof(W)};
			return GetLastEntity(destroy, types);
		}

		/**
		* Returns a list of registered components.
		*/
		public List<T> GetComponents<T>() where T:Component {
			Type type = typeof(T);

			if(!componentMap.ContainsKey(type)) {
				return new List<T>();
			}

			var subsection = componentMap[type].GetRange(0, componentMap[type].Count);
			return subsection.ConvertAll(x => (T)x);
		}

		/**
		* Returns a tuple of list of registered components where Components exist on the same entity
		*/
		public Tuple<T,U> GetComponent<T,U>() where T:Component where U:Component{

			var entities = GetEntities<T,U>();

			Type typeT = typeof(T);
			Type typeU = typeof(U);

			if(!componentMap.ContainsKey(typeT)) {
				componentMap[typeT] = new List<Component>();
			}

			if(!componentMap.ContainsKey(typeU)) {
				componentMap[typeU] = new List<Component>();
			}

			var subsectionT = componentMap[typeT].GetRange(0, componentMap[typeT].Count).ConvertAll(x => (T)x);
			var subsectionU = componentMap[typeU].GetRange(0, componentMap[typeU].Count).ConvertAll(x => (U)x);

			var entitiesT = subsectionT.Where(x => entities.Contains(x.gameObject));
			var entitiesU = subsectionU.Where(x => entities.Contains(x.gameObject));

			return new Tuple<T,U>(entitiesT.First(), entitiesU.First());
		}

		/**
		* Returns a tuple of list of registered components where Components exist on the same entity
		*/
		public Tuple<T,U,V> GetComponent<T,U,V>() where T:Component where U:Component where V:Component {

			var entities = GetEntities<T,U,V>();

			Type typeT = typeof(T);
			Type typeU = typeof(U);
			Type typeV = typeof(V);

			if(!componentMap.ContainsKey(typeT)) {
				componentMap[typeT] = new List<Component>();
			}

			if(!componentMap.ContainsKey(typeU)) {
				componentMap[typeU] = new List<Component>();
			}

			if(!componentMap.ContainsKey(typeV)) {
				componentMap[typeV] = new List<Component>();
			}

			var subsectionT = componentMap[typeT].GetRange(0, componentMap[typeT].Count).ConvertAll(x => (T)x);
			var subsectionU = componentMap[typeU].GetRange(0, componentMap[typeU].Count).ConvertAll(x => (U)x);
			var subsectionV = componentMap[typeV].GetRange(0, componentMap[typeU].Count).ConvertAll(x => (V)x);

			var entitiesT = subsectionT.Where(x => entities.Contains(x.gameObject));
			var entitiesU = subsectionU.Where(x => entities.Contains(x.gameObject));
			var entitiesV = subsectionV.Where(x => entities.Contains(x.gameObject));

			return new Tuple<T,U,V>(entitiesT.First(), entitiesU.First(), entitiesV.First());
		}

		/**
		* Returns a tuple of list of registered components where Components exist on the same entity
		*/
		public Tuple<List<T>,List<U>> GetComponents<T,U>() where T:Component where U:Component{

			var entities = GetEntities<T,U>();

			Type typeT = typeof(T);
			Type typeU = typeof(U);

			if(!componentMap.ContainsKey(typeT)) {
				componentMap[typeT] = new List<Component>();
			}

			if(!componentMap.ContainsKey(typeU)) {
				componentMap[typeU] = new List<Component>();
			}

			var subsectionT = componentMap[typeT].GetRange(0, componentMap[typeT].Count).ConvertAll(x => (T)x);
			var subsectionU = componentMap[typeU].GetRange(0, componentMap[typeU].Count).ConvertAll(x => (U)x);

			var entitiesT = subsectionT.Where(x => entities.Contains(x.gameObject)).ToList();
			var entitiesU = subsectionU.Where(x => entities.Contains(x.gameObject)).ToList();

			return new Tuple<List<T>,List<U>>(entitiesT, entitiesU);
		}

		/**
		* Returns a tuple of list of registered components where Components exist on the same entity
		*/
		public Tuple<List<T>,List<U>,List<V>> GetComponents<T,U,V>() where T:Component where U:Component where V:Component{

			var entities = GetEntities<T,U,V>();

			Type typeT = typeof(T);
			Type typeU = typeof(U);
			Type typeV = typeof(V);

			if(!componentMap.ContainsKey(typeT)) {
				componentMap[typeT] = new List<Component>();
			}

			if(!componentMap.ContainsKey(typeU)) {
				componentMap[typeU] = new List<Component>();
			}

			if(!componentMap.ContainsKey(typeV)) {
				componentMap[typeV] = new List<Component>();
			}

			var subsectionT = componentMap[typeT].GetRange(0, componentMap[typeT].Count).ConvertAll(x => (T)x);
			var subsectionU = componentMap[typeU].GetRange(0, componentMap[typeU].Count).ConvertAll(x => (U)x);
			var subsectionV = componentMap[typeU].GetRange(0, componentMap[typeV].Count).ConvertAll(x => (V)x);

			var entitiesT = subsectionT.Where(x => entities.Contains(x.gameObject)).ToList();
			var entitiesU = subsectionU.Where(x => entities.Contains(x.gameObject)).ToList();
			var entitiesV = subsectionV.Where(x => entities.Contains(x.gameObject)).ToList();

			return new Tuple<List<T>,List<U>,List<V>>(entitiesT, entitiesU, entitiesV);
		}

		/**
		* Returns a list of registered components filtered by a custom linq search
		*/
		public List<T> GetComponents<T>(Predicate<T> search) where T:Component{
			Type type = typeof(T);

			if(!componentMap.ContainsKey(type)) {
				return new List<T>();
			}

			var subsection = componentMap[type].GetRange(0, componentMap[type].Count);
			var list = subsection.ConvertAll(x => (T)x);

			return list.FindAll(search);
		}

		/**
		* Returns the first registered component else null
		*/
		public T GetComponent<T>() where T:Component{

			Type type = typeof(T);

			if(!componentMap.ContainsKey(type) || componentMap[type].Count == 0) {
				return default(T);
			}

			return (T) Convert.ChangeType(componentMap[type][0], type);
		}

		/**
		* Returns a list of registered components filtered by a custom linq search
		*/
		public T GetComponent<T>(Predicate<T> search) where T:Component{
			Type type = typeof(T);

			if(!componentMap.ContainsKey(type) || componentMap[type].Count == 0) {
				return default(T);
			}

			var subsection = componentMap[type].GetRange(0, componentMap[type].Count);
			var list = subsection.ConvertAll(x => (T)x);

			return list.Find(search);
		}

		/**
		* Returns the first registered Entity with the component T
		*/
		public GameObject GetEntity<T>() where T:Component {

			Type type = typeof(T);

			if(!componentMap.ContainsKey(type) || componentMap[type].Count == 0) {
				return null;
			}

			return (GameObject) componentMap[type][0].gameObject;
		}

		/**
		* Returns the first registered Entity with the component T
		* with the addtion of a custom linq filter
		*/
		public GameObject GetEntity<T>(Predicate<GameObject> search) where T:Component {

			Type type = typeof(T);

			if(!componentMap.ContainsKey(type) || componentMap[type].Count == 0) {
				return null;
			}

			var subsection = componentMap[type].GetRange(0, componentMap[type].Count);
			var list = subsection.ConvertAll(x => x.gameObject);

			return list.Find(search);
		}

		/**
		* Returns the first registered Entity else null
		*/
		public GameObject GetEntity(Type type) {

			if(!componentMap.ContainsKey(type) || componentMap[type].Count == 0) {
				return null;
			}

			return (GameObject) componentMap[type][0].gameObject;
		}

		/**
		* Returns a list of entities that posses T
		*/
		public List<GameObject> GetEntities<T>() where T:Component {
			return GetEntities(typeof(T));
		}

		/**
		* Returns a list of entities that posses T
		* with a custom linq filter
		*/
		public List<GameObject> GetEntities<T>(Predicate<GameObject> search) where T:Component{

			Type type = typeof(T);

			if(!componentMap.ContainsKey(type) || componentMap[type].Count == 0) {
				return null;
			}

			var subsection = componentMap[type].GetRange(0, componentMap[type].Count);
			var list = subsection.ConvertAll(x => x.gameObject);

			return list.FindAll(search);
		}

		/**
		* Returns a list of entities that posses a recipe of components
		*/
		public List<GameObject> GetEntities(params Type[] componentTypes){

			List<GameObject> potential = new List<GameObject>();
			List<GameObject> matches = new List<GameObject>();

			foreach(Type type in componentTypes){
					if(entityMap.ContainsKey(type)){
						List<GameObject> entities = entityMap[type];

					if(entities != null){
						potential.AddRange(entities);
					}
				}
			}

			foreach(GameObject entity in potential){
				if(HasComponents(entity, componentTypes)){
					matches.Add(entity);
				}
			}

			return matches;
		}

		/**
		* Returns a list of entities that posses a recipe of 2 components
		*/
		public List<GameObject> GetEntities<T,U>() where T:Component where U:Component {

			var types = new Type[2]{typeof(T), typeof(U)};
			return GetEntities(types);
		}

		/**
		* Returns a list of entities that posses a recipe of 2 components
		*/
		public List<GameObject> GetEntities<T,U>(Predicate<GameObject> search) where T:Component where U:Component {

			var types = new Type[2]{typeof(T), typeof(U)};
			return (List<GameObject>) GetEntities(types).FindAll(search);
		}

		/**
		* Returns a list of entities that posses a recipe of 3 components
		*/
		public List<GameObject> GetEntities<T,U,V>() where T:Component where U:Component where V:Component{

			var types = new Type[3] {typeof(T), typeof(U), typeof(V)};
			return GetEntities(types);
		}

		/**
		* Returns a list of entities that posses a recipe of 3 components
		*/
		public List<GameObject> GetEntities<T,U,V>(Predicate<GameObject> search) where T:Component where U:Component where V:Component {

			var types = new Type[3]{typeof(T), typeof(U), typeof(V)};
			return (List<GameObject>) GetEntities(types).FindAll(search);
		}

		/**
		* Returns a list of entities that posses a recipe of 4 components
		*/
		public List<GameObject> GetEntities<T,U,V,W>() where T:Component where U:Component where V:Component where W:Component{

			var types = new Type[4] {typeof(T), typeof(U), typeof(V), typeof(W)};
			return GetEntities(types);
		}

		/**
		* Returns a list of entities that posses a recipe of 4 components
		*/
		public List<GameObject> GetEntities<T,U,V,W>(Predicate<GameObject> search) where T:Component where U:Component where V:Component where W:Component {

			var types = new Type[4]{typeof(T), typeof(U), typeof(V), typeof(W)};
			return (List<GameObject>) GetEntities(types).FindAll(search);
		}

		/**
		* Does an entity posses a recipe of components
		*/
		public bool HasComponents(GameObject entity, Type[] componentTypes){

			foreach(Type type in componentTypes){
				if(!entityMap.ContainsKey(type)){
					return false;
				}

				List<GameObject> entities = entityMap[type];

				if(entities == null || !entities.Contains(entity)){
					return false;
				}
			}

			return true;
		}

		/**
		* Does an entity posses a recipe of 2 components
		*/
		public bool HasComponents<T, U>(GameObject entity) where T:Component where U:Component{

			var types = new Type[2]{typeof(T), typeof(U)};
			return HasComponents(entity, types);
		}

		/**
		* Does an entity posses a recipe of 3 components
		*/
		public bool HasComponents<T,U,V>(GameObject entity) where T:Component where U:Component where V:Component{

			var types = new Type[3]{typeof(T), typeof(U), typeof(V)};
			return HasComponents(entity, types);
		}

		/**
		* Does an entity posses a recipe of 4 components
		*/
		public bool HasComponents<T,U,V,W>(GameObject entity) where T:Component where U:Component where V:Component where W:Component{

			var types = new Type[4]{typeof(T), typeof(U), typeof(V), typeof(W)};
			return HasComponents(entity, types);
		}

		public Type[] GetComponentTypes<T,U>() where T:Component where U:Component{
			return new Type[2]{typeof(T), typeof(U)};
		}

		public Type[] GetComponentTypes<T,U,V>() where T:Component where U:Component where V:Component{
			return new Type[3]{typeof(T), typeof(U), typeof(V)};
		}

		public Type[] GetComponentTypes<T,U,V,W>() where T:Component where U:Component where V:Component where W:Component{
			return new Type[4]{typeof(T), typeof(U), typeof(V), typeof(W)};
		}

		/**
		* Removes a component from the Entity List
		* @TODO theres still an issue with trying remove components that arent Registered
		* inside the EntityList
		*/
		public void RemoveComponent(Component component, bool destroy = true){

			//entity map
			Type type = component.GetType();

			if(entityMap.ContainsKey(type) && entityMap[type] != null && entityMap[type].Contains(component.gameObject)){
				entityMap[type].Remove(component.gameObject);
			}

			//components map
			if(componentMap.ContainsKey(type) && componentMap[type] != null && componentMap[type].Contains(component)){
				componentMap[type].Remove(component);
			}

			//remove the component from the entity
			if(destroy){
				MonoBehaviour.Destroy(component);
			}
		}

		/**
		* Add a component to the Entity List
		*/
		public void AddComponent(Component component){

			if(component == null)
				return;

			//entity map
			Type type = component.GetType();

			if(!entityMap.ContainsKey(type)){
				entityMap[type] = new List<GameObject>();
			}

			if(!entityMap[type].Contains(component.gameObject)){
				entityMap[type].Add(component.gameObject);
			}

			//components map
			if(!componentMap.ContainsKey(type)){
				componentMap[type] = new List<Component>();
			}

			if(!componentMap[type].Contains(component)){
				componentMap[type].Add(component);
			}
		}

		/**
		* List all component types
		*/
		public void OutputComponentTypes(){

			Debug.Log("<color=yellow>----- Registered Component Types -----</color>");

			foreach(KeyValuePair<Type, List<Component>> entry in componentMap)
			{
				Debug.Log("Count:<color=yellow>" + entry.Value.Count + "</color>, Type:<color=orange>" + entry.Key.Name + "</color>" );
			}

			Debug.Log("<color=yellow>--------------------------------------</color>");
		}


		/**
		* Returns the first registered Entity with a component that
		* implements interface T
		*/
		public GameObject GetIEntity<T>() where T:class {

			Type type = typeof(T);

			if(type.IsInterface){

				var pairs = entityMap.Where(pair => pair.Key.GetInterfaces().Contains(type));

				if(pairs.Count() > 0){
					var kvp = pairs.First();
					if(kvp.Value.Count == 0){
						return null;
					}

					return kvp.Value[0];

				}
			}

			return null;
		}

		/**
		* Returns all registered Entities with a component that
		* implements interface T
		*/
		public List<GameObject> GetIEntities<T>() where T:class {

			Type type = typeof(T);

			if(type.IsInterface){

				//IEnumerable of KeyValuePair<Type, List<GameObject>>()
				var pairs = entityMap.Where(pair => pair.Key.GetInterfaces().Contains(type));

				//IEnumerable of List<GameObject>
				var values = pairs.Select(pair => pair.Value);

				//Create list of all GameObjects
				var gos = new List<GameObject>();
				foreach(List<GameObject> item in values){
					gos.AddRange(item);
				}

				return gos;
			}

			return null;
		}

		/**
		* Returns all registered Components that implements interface T
		*/
		public List<T> GetIComponents<T>() where T:class {

			Type type = typeof(T);

			if(type.IsInterface){

				//IEnumerable of KeyValuePair<Type, List<GameObject>>()
				var pairs = componentMap.Where(pair => pair.Key.GetInterfaces().Contains(type));

				//IEnumerable of List<Component>
				var values = pairs.Select(pair => pair.Value);

				//Create list of all Component
				var components = new List<Component>();
				foreach(List<Component> item in values){
					components.AddRange(item);
				}

				return new List<T>(components.Cast<T>());
			}

			return null;
		}

		/**
		* Returns the first registered component that
		* implements interface T
		*/
		public T GetIComponent<T>() where T:class {

			Type type = typeof(T);

			if(type.IsInterface){

				var pairs = componentMap.Where(pair => pair.Key.GetInterfaces().Contains(type));

				if(pairs.Count() > 0){
					var kvp = pairs.First();
					if(kvp.Value.Count == 0){
						return null;
					}

					return kvp.Value[0] as T;

				}
			}

			return null;
		}
	}
}
