//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nonatomic.Utomic {

	public abstract class Signal {

		private List<ICommand> handlers = new List<ICommand>();
		private List<ICommand> onceHandlers = new List<ICommand>();
		private List<IGuard> guards = new List<IGuard>();
		private Dictionary<IGuard, Signal> failGuards = new Dictionary<IGuard, Signal>();
		public List<Type> paramTypes = new List<Type>();

		protected void Restrict<T>(){
			paramTypes.Add(typeof(T));
		}

		protected Signal Restrict<T,U>(){
			paramTypes.AddRange (new Type[]{typeof(T),typeof(U)});
			return this;
		}

		protected Signal Restrict<T,U,V>(){
			paramTypes.AddRange (new Type[]{typeof(T),typeof(U),typeof(V)});
			return this;
		}

		protected Signal Restrict<T,U,V,W>(){
			paramTypes.AddRange (new Type[]{typeof(T),typeof(U),typeof(V), typeof(W)});
			return this;
		}

		protected Signal Restrict<T,U,V,W,X>(){
			paramTypes.AddRange (new Type[]{typeof(T),typeof(U),typeof(V), typeof(W), typeof(X)});
			return this;
		}

		protected Signal Restrict<T,U,V,W,X,Y>(){
			paramTypes.AddRange (new Type[]{typeof(T),typeof(U),typeof(V), typeof(W), typeof(X), typeof(Y)});
			return this;
		}

		public Signal Listen(ICommand command) {
			handlers.Add(command);
			return this;
		}

		public Signal AddGuard(IGuard guard){
			guards.Add(guard);
			return this;
		}

		public Signal AddGuard(IGuard guard, Signal fail){
			failGuards.Add(guard, fail);
			return this;
		}

		public Signal AddGuard<T>() where T:IGuard {
			this.Bind<T>();
			guards.Add(this.Inject<T>());
			return this;
		}

		public Signal AddGuard<T,U>() where T:IGuard where U:Signal{
			this.Bind<T>();
			failGuards.Add(this.Inject<T>(), this.Inject<U>());
			return this;
		}

		public Signal RemoveGuard(IGuard guard){
			if(guards.Contains(guard)){
				guards.Remove(guard);
			}

			if(failGuards.ContainsKey(guard)){
				failGuards.Remove(guard);
			}

			return this;
		}

		public Signal RemoveAllGuards(){
			guards.Clear();
			failGuards.Clear();
			return this;
		}

		public Signal ListenOnce(ICommand command) {
			onceHandlers.Add(command);
			return this;
		}

		public Signal Remove(ICommand command) {
			if(handlers.Contains(command)){
			   handlers.Remove(command);
			}

			if(onceHandlers.Contains(command)){
				onceHandlers.Remove(command);
			}

			return this;
		}

		public Signal RemoveAll(){
			handlers.Clear();
			onceHandlers.Clear();

			return this;
		}

		private string ParamListString(object[] parameters){
			string str = "[";
			foreach(object o in parameters){
				str += o.GetType().Name;
				str += ", ";
			}
			str += "]";

			return str;
		}

		private string ParamTypeString(List<Type> parameters){
			string str = "[";
			foreach(Type t in parameters){
				str += t.Name;
				str += ", ";
			}
			str += "]";

			return str;
		}

		public Signal Dispatch(params object[] parameters) {

			int count = 0;
			foreach(object param in parameters){
				if(parameters.Length == paramTypes.Count){
					if(parameters.Length > 0){
						Type paramType = param.GetType();
						Type targetType = paramTypes[count];

						bool isTargetClass = targetType.IsClass;

						//target type is a class
						if(isTargetClass){

							//check class type or base class type matches
							bool typeCheck = paramType != paramTypes[count];
							bool baseTypeCheck = paramType.BaseType != targetType;

							if(typeCheck && baseTypeCheck){
								this.LogError(" Signal: " + this.GetType().Name + " params type exception. Expected:" + paramTypes[count].GetType().Name + " Recieved:" + param.GetType().Name );
							}
						}

						//target class is an interface
						else{

							//checkthat the paramater received can be assigned to
							//to the target interface
							if(!targetType.IsAssignableFrom(paramType)){
								this.LogError(" Signal: " + this.GetType().Name + " params type exception. Expected:" + paramTypes[count].GetType().Name + " Does not implement interface:" + param.GetType().Name );
							}
						}

					}
				}
				else{

					string paramListStr = ParamListString(parameters);
					string paramTypeStr = ParamTypeString(paramTypes);

					this.LogError(" Signal: " + this.GetType().Name + " param count exception. Expected:" + paramTypeStr + " Received:" + paramListStr);
				}

				count++;
			}

			foreach(IGuard guard in guards){
				if(!guard.Execute(parameters)){
					this.Log("GUARD FAILED:" + guard);
					return this;
				}
			}

			foreach(KeyValuePair<IGuard, Signal> kvp in failGuards){
				if(!kvp.Key.Execute(parameters)){
					this.Log("GUARD FAILED:" + kvp.Key + ", FALLS BACK TO:" + kvp.Value);
					if(kvp.Value != null){
						kvp.Value.Dispatch();
					}
					return this;
				}
			}

			for(int i = handlers.Count - 1; i >= 0; i--) {
				handlers[i].Execute(parameters);
			}

			//Use for loop over foreach as signals may be added whilst iterating
			for(int i = onceHandlers.Count - 1; i >= 0; i--) {
				onceHandlers[i].Execute(parameters);
				onceHandlers.RemoveAt(i);
			}

			return this;
		}
	}
}
