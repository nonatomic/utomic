//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//
using UnityEngine;

namespace Nonatomic.Utomic {

	public class CommandMap : ICommandMap {

		/**
		*  Every time T is dispatched U is Executed
		*/
		public Signal Bind<T, U>() where T:Signal where U:ICommand{
			this.Bind<T>();
			this.Bind<U>();
			var signal = this.Inject<T>();
			var command = this.Inject<U>();
			signal.Listen(command);
			return signal;
		}

		/**
		*  When T is dispatched U is Executed only once
		*/
		public Signal BindOnce<T, U>() where T:Signal where U:ICommand{
			this.Bind<T>();
			this.Bind<U>();
			var signal = this.Inject<T>();
			var command = this.Inject<U>();
			signal.ListenOnce(command);
			return signal;
		}

		/**
		*  Every time T is dispatched U is Executed
		*  Uses Injector ReBind to remove existing instances of T and U
		*/
		public Signal Rebind<T, U>() where T:Signal where U:ICommand{
			this.Rebind<T>();
			this.Rebind<U>();
			var signal = this.Inject<T>();
			var command = this.Inject<U>();
			signal.Listen(command);
			return signal;
		}

		/**
		*  When T is dispatched U is Executed only once
		*  Uses Injector ReBind to remove existing instances of T and U
		*/
		public Signal RebindOnce<T, U>() where T:Signal where U:ICommand{
			this.Rebind<T>();
			this.Rebind<U>();
			var signal = this.Inject<T>();
			var command = this.Inject<U>();
			signal.ListenOnce(command);
			return signal;
		}
	}
}
