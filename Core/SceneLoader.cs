//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Nonatomic.Utomic {

	public class SceneLoader : ISceneLoader {

		public void LoadSingle(string scene, Signal progress, Signal complete) {
			CoroutineRunner runner = this.EntityList().GetComponent<CoroutineRunner>();
			runner.Run(LoadScene(scene, LoadSceneMode.Single, progress, complete));
		}

		public void LoadAdditive(string scene, Signal progress, Signal complete) {
			CoroutineRunner runner = this.EntityList().GetComponent<CoroutineRunner>();
			runner.Run(LoadScene(scene, LoadSceneMode.Additive, progress, complete));
		}

		private IEnumerator LoadScene(string scene, LoadSceneMode mode, Signal progress, Signal complete) {
			AsyncOperation async = SceneManager.LoadSceneAsync(scene, mode);
			while(!async.isDone) {
				if(progress != null){
					progress.Dispatch(scene, async.progress);
				}
				yield return async.isDone;
			}

			if(complete != null){
				progress.Dispatch(scene, async.progress);
				complete.Dispatch(scene, async.progress);
			}
		}
	}
}
