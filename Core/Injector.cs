//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Nonatomic.Utomic {


	public class Injector {

		//Singleton
		static readonly Injector instance = new Injector();

		public static Injector Instance {
		 	get{return instance;}
		}

		public Dictionary<Type,Type> typeMap = new Dictionary<Type, Type>();
		public Dictionary<Type, object> singletonMap = new Dictionary<Type, object>();

		/**
		* Bind Type T to Type U
		* Call to Inject T returns Singleton Instance U
		*/
		public void Bind<T, U>() {
			typeMap[typeof(T)] = typeof(U);
		}

		/**
		* Bind Type T to instance T
		* Call to Inject T returns Singleton Instance T
		*/
		public void Bind<T>(T instance) {
			Type type = typeof(T);
			typeMap[type] = type;
			singletonMap[type] = instance;
		}

		/**
		* Bind Type T to Type T
		* Call to Inject T returns Singleton Instance T
		*/
		public void Bind<T>() {
			typeMap[typeof(T)] = typeof(T);
		}

		/**
		* Bind Type T to Type T
		* Call to Inject T returns Singleton Instance T
		* Removes existing instance of T
		*/
		public void Rebind<T>() {
			Type type = typeof(T);
			typeMap[type] = typeof(T);
			singletonMap.Remove(type);
		}

		/**
		* Bind Type T to Type U
		* Call to Inject T returns Singleton Instance U
		*/
		public void Rebind<T, U>() {
			Type type = typeof(T);
			typeMap[typeof(T)] = typeof(U);
			singletonMap.Remove(type);
		}

		/**
		* Bind Type T to instance T
		* Call to Inject T returns Singleton Instance T
		* Removes existing instance of T
		*/
		public void Rebind<T>(T instance) {
			Type type = typeof(T);
			typeMap[type] = type;
			singletonMap.Remove(type);
			singletonMap[type] = instance;
		}

		public bool HasBinding<T>(){
			return typeMap.ContainsKey(typeof(T));
		}

		/**
		*	Provides a singleton instance of T
		*/
		public T Inject<T>() {
			Type type = typeof(T);

			if(singletonMap.ContainsKey(type)) {
				return (T)singletonMap[type];
			}
			else if(typeMap.ContainsKey(type)) {
				singletonMap[type] = Activator.CreateInstance(typeMap[type]);

				#pragma warning disable 0168 //disable warning for not using e
				try {
					return (T)singletonMap[type];
				}
				catch (InvalidCastException e) {
					Debug.LogError("InjectorError: Attempting to inject " + singletonMap[type].GetType().Name + " into type "+ type.Name);
				}
			}
			else {
				Debug.LogWarning("InjectorError: No binding found for " + type.ToString() );
			}

			return default(T);
		}

		/**
		*	Provides a singleton instance by type name - requires casting
		*/
		public object Inject(string typeName) {

			var type = singletonMap.FirstOrDefault(x => x.Key.Name == typeName).Key;

			if(singletonMap.ContainsKey(type)) {
				return (object)singletonMap[type];
			}
			else if(typeMap.ContainsKey(type)) {
				singletonMap[type] = Activator.CreateInstance(typeMap[type]);

				#pragma warning disable 0168 //disable warning for not using e
				try {
					return (object)singletonMap[type];
				}
				catch (InvalidCastException e) {
					Debug.LogError("InjectorError: Attempting to inject " + singletonMap[type].GetType().Name + " into type "+ type.Name);
				}
			}
			else {
				Debug.LogWarning("InjectorError: No binding found for " + type.ToString() );
			}

			return null;
		}
	}
}
