//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Nonatomic.Utomic {

	/**
	* The EntityPool is a place to store GameObjects whilst not in use.
	* Entities stored here will be removed from the EntityList until they are
	* removed, at which point they will be readded to EntityList
	*/
	public class EntityPool : IEntityPool {

		private Dictionary<Type, List<GameObject>> entityMap = new Dictionary<Type, List<GameObject>>();
		private Dictionary<Type, List<Component>> componentMap = new Dictionary<Type, List<Component>>();
		private List<GameObject> entities = new List<GameObject>();

		public void Push(GameObject entity) {

			entity.SetActive(false);

			var components = entity.GetComponents<Component>();
			foreach (Component component in components) {
				if(!entities.Contains(entity)){
					entities.Add(entity);
				}
				AddComponent(component);
			}

			this.EntityList().RemoveEntity(entity, false);
		}

		public void Push(List<GameObject> entities) {

			foreach(GameObject entity in entities){
				Push(entity);
				this.EntityList().RemoveEntity(entity, false);
			}
		}

		/**
		*	Remove and return the first entity found with T Component
		*/
		public GameObject Pop<T>() where T:Component {
			List<T> components = GetComponents<T>();
			GameObject entity = null;
			if(components.Count > 0){
				entity = components.Last().gameObject;
				entity.SetActive(true);
				RemoveEntity(entity);
			}

			if(entity != null)
				this.EntityList().AddEntity(entity);

			return entity;
		}

		/**
		* search components with linq
		*/
		public GameObject Pop<T>(Predicate<T> search) where T:Component {
			List<T> components = GetComponents<T>();

			GameObject entity = null;

			if(components.Count > 0){
				var result = components.Find(search);
				if(result != null)
					entity = result.gameObject;
			}

			if(entity != null){
				entity.SetActive(true);
				RemoveEntity(entity);
				this.EntityList().AddEntity(entity);
			}


			return entity;
		}

		/**
		* search all entities with a linq query
		*/
		public GameObject Pop(Predicate<GameObject> search) {

			var entity = entities.Find(search);

			if(entity != null){
				entity.SetActive(true);
				RemoveEntity(entity);
				this.EntityList().AddEntity(entity);
			}

			return entity;
		}

		/**
		* Removes & destroys the last entity registered with all component types
		*/
		public GameObject Pop(params Type[] componentTypes) {
			List<GameObject> entities = GetEntities(componentTypes);
			GameObject entity = null;
			if(entities.Count > 0){
				entity = entities.Last();
				RemoveEntity(entity);
			}

			if(entity != null)
				this.EntityList().AddEntity(entity);

			return entity;
		}

		/**
		* Add a component to the Entity List
		*/
		private void AddComponent(Component component){

			//entity map
			Type type = component.GetType();

			if(!entityMap.ContainsKey(type)){
				entityMap[type] = new List<GameObject>();
			}

			if(!entityMap[type].Contains(component.gameObject)){
				entityMap[type].Add(component.gameObject);
			}

			//components map
			if(!componentMap.ContainsKey(type)){
				componentMap[type] = new List<Component>();
			}

			if(!componentMap[type].Contains(component)){
				componentMap[type].Add(component);
			}
		}

		/**
		* Remove the paramater Entity from the EntityList
		*/
		private void RemoveEntity(GameObject entity) {
			var components = entity.GetComponents<Component>();
			foreach (Component component in components) {
				if(entities.Contains(entity)){
					entities.Remove(entity);
				}
				RemoveComponent(component, false);
			}
		}

		/**
		* Returns a list of registered components.
		*/
		private List<T> GetComponents<T>() where T:Component {
			Type type = typeof(T);

			if(!componentMap.ContainsKey(type)) {
				return new List<T>();
			}

			var subsection = componentMap[type].GetRange(0, componentMap[type].Count);
			return subsection.ConvertAll(x => (T)x);
		}

		/**
		* Returns a list of entities that posses a recipe of components
		*/
		private List<GameObject> GetEntities(params Type[] componentTypes){

			List<GameObject> potential = new List<GameObject>();
			List<GameObject> matches = new List<GameObject>();

			foreach(Type type in componentTypes){
					if(entityMap.ContainsKey(type)){
						List<GameObject> entities = entityMap[type];

					if(entities != null){
						potential.AddRange(entities);
					}
				}
			}

			foreach(GameObject entity in potential){
				if(HasComponents(entity, componentTypes)){
					matches.Add(entity);
				}
			}

			return matches;
		}

		/**
		* Does an entity posses a recipe of components
		*/
		private bool HasComponents(GameObject entity, Type[] componentTypes){

			foreach(Type type in componentTypes){
				if(!entityMap.ContainsKey(type)){
					return false;
				}

				List<GameObject> entities = entityMap[type];

				if(entities == null || !entities.Contains(entity)){
					return false;
				}
			}

			return true;
		}

		/**
		* Removes a component from the Entity List
		*/
		private void RemoveComponent(Component component, bool destroy = true){

			//entity map
			Type type = component.GetType();

			if(entityMap[type] != null && entityMap[type].Contains(component.gameObject)){
				entityMap[type].Remove(component.gameObject);
			}

			//components map
			if(componentMap[type] != null && componentMap[type].Contains(component)){
				componentMap[type].Remove(component);
			}
		}
	}
}
