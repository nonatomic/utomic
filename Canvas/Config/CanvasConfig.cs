//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using System;
using UnityEngine.UI;

namespace Nonatomic.Utomic {

	public class CanvasConfig {

		public string name;
		public RenderMode renderMode;
		public bool pixelPerfect;
		public bool eventSystem;
		public CanvasScaler.ScaleMode scaleMode;
		public Vector2 referenceResolution;
		public CanvasScaler.ScreenMatchMode screenMatchModel;
		public float matchWidthOrHeight;
		public Camera camera;
		public int planeDistance;

		public CanvasConfig() {
			name = "Canvas";
			renderMode = RenderMode.ScreenSpaceOverlay;
			pixelPerfect = true;
			eventSystem = true;
			screenMatchModel = CanvasScaler.ScreenMatchMode.MatchWidthOrHeight;
			scaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;//ConstantPhysicalSize || ConstantPixelSize || ScaleWithScreenSize
			referenceResolution = new Vector2(640,1136);
			matchWidthOrHeight = 0.5f;
			planeDistance = 90;
		}
	}
}
