//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Nonatomic.Utomic {

	public class CanvasFactory : ICanvasFactory {

		public Canvas Make(CanvasConfig config, GameObject parent) {

			var go = new GameObject(config.name);
			go.transform.SetParent(parent.transform);

			var canvas = go.AddEntityComponent<Canvas>();
			canvas.renderMode = config.renderMode;
			canvas.pixelPerfect = config.pixelPerfect;
			canvas.planeDistance = config.planeDistance;
			// canvas.worldCamera = camera;

			var scaler = go.AddEntityComponent<CanvasScaler>();
			scaler.uiScaleMode = config.scaleMode;
			scaler.referenceResolution = config.referenceResolution;
			scaler.screenMatchMode = config.screenMatchModel;
			scaler.matchWidthOrHeight= config.matchWidthOrHeight;

			go.AddEntityComponent<GraphicRaycaster>();
			go.AddEntityComponent<CanvasDepthManager>();

			if(config.eventSystem){
				go.AddEntityComponent<EventSystem>();
				go.AddEntityComponent<StandaloneInputModule>();
			}

			this.EntityList().AddEntity(go);
			return canvas;
		}

		public T Make<T>(CanvasConfig config, GameObject parent) where T : Component {
			T component = Make(config, parent).gameObject.AddEntityComponent<T>();
			this.EntityList().AddComponent(component);
			return component;
		}
	}
}
