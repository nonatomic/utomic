Read Me
=======

### *Utomic*

The Utomic framework is a scalable Unity C# game platform.

#### *Features*

-	Force SoC via featuring
-	Flexible comms via Signal -> Command
-	IoC via runtime Injector
-	Convenience methods
-	Unit testing
-	Bench marking
-	Debugging

#### *Spec*

-	Version 0.9.1
-	Requires Unity 5.3.1+

#### *Support*

email: support@nonatomic.co.uk

### Using Utomic

#### *Features*

Features provide a means of separating literal game features into self contained, reusable units.

Features are integrated in four stages. Wire, Structure, Setup and Run.

###### *Wire*

This is where you `Wire` up your models, signals, commands and managers

###### *Structure*

This is where you create initial infrastructure from non preloaded assets. This could be containers, cameras, lights. Things that need to exist in the world before your application starts.

###### *Setup*

This is where you might set initial values. Instantiate data objects. Setup all dependancies prior to the application running

###### *Run*

This is where you do any early graphics work. At this stage all assets are loaded, all infrastructure is setup and all dependencies are available.

#### *Getting Setup*

Utomic is super simple to get up and running with.

1.	Ensure the Utomic library is included in your Unity project.
2.	Create an empty GameObject and attach a new script. This will be your entry point.
5.	In your entry script add the using statement `using Nonatomic.Utomic;`
6.	Then add an Awake method. In here place `this.AddUtomic();`
7.	Bingo! you now have Utomic setup.

#### *Adding Features*

Features should be added in your entry script as follows.

`this.AddFeature<PreloaderFeature>();`

#### *Injection*

Utomic utilises a run time injector to handle singletons. Currently the injector does not support multiple instances.

Utomic provides some syntactic sugar to access the injector.

To get the most out of injection and to keep your code as decoupled as possible bind your classes by interface.

*Example:*

```
this.Bind<IMyClass, MyClass>();
```

The injector is NOT intended to store references to GameObjects. See EntityList.

The best places to setup your bindings are in the Wire method of your features. If you have universally used bindings you should either create a Core feature or add your bindings in your entry script.

#### *Signals & Commands*

Utomic uses a unique messaging system that discourages inter class communication. Instead methods are extracted into self contained classes known as commands. This provides a highly flexible, decoupled codebase.

Utomic provides syntactic sugar for mapping Signals to Commands.

*Example:*

```
this.CommandMap().Bind<MySignal, MyCommand>();
```

##### *Commands*

Commands should contain logic for a single purpose. They receive a list of parameters registered in each signal.

*Example:*

```
public class ScorePointsCommand : ICommand {

	public void Execute(params object[] parameters) {

		var points = (int)parameters[0];
		var scoreModel = this.Inject<IScoreModel>();

		scoreModel.point += points;
	}
```

##### *Signals*

Signals provide a flexible means of calling Commands. This is because you can wire up a signal to any command. As an example in your game on game over you may dispatch your GameOverSignal, which you may start by mapping to your GoToScoreboardCommand. However as your game develops you realise on game over you need to go to a score breakdown screen. So you just rewire your GameOverSignal as follows.

```
this.CommandMap().Bind<GameOverSignal, GoToScoreBreakDownCommand>();
```

If you wish to send data through your signal this is achieved by passing upto 5 class Types to the Signals `Restrict` method.

*Example:*

```
public class ExampleSignal : Signal {

	public ExampleSignal() {
		this.Restrict<string, int, bool, MyCustomType, float>();
	}
```

Settings restrictions provide helpful error messages should you pass the wrong data types through your signal.

*Example:*

```
this.Inject<MySignal>().Dispatch(1.0f, "testing", new MyCustomType());
```

#### *Entity List*

The EntityList is a means of storing references to GameObjects. Providing a fast searchable index of existing GameObjects. Searchable by Component type, Predicate query or recipes of Components.

Utomic provides syntactic sugar for access to the EntityList.

*Example:*

```
//simple searches of the EntityList
GameObject entity = this.EntityList().GetEntity<MyCustomComponent>();
List<GameObject> entities = this.EntityList().GetEntities<MyCustomComponent>();
MyCustomComponent component = this.EntityList().GetComponent<MyCustomComponent>();

//complex searches of the EntityList
GameObject entity = this.EntityList().GetEntity<MyCustomComponent>(x => x.name = "MyEntityName");
List<GameObject> entities = this.EntityList().GetEntities(typeof(MyCustomComponent), typeof(RectTransform));

```

One thing to bear in mind is that the EntityList is not psychic. If you add or remove a component you must update the reference to the Entity. This can be achieved by simply re-adding the entity to the list.

```
this.EntityList().AddEntity(myEntity);
```

This will replace the entities reference and update which Components its mapped against. You can also use;

```
this.EntityList().AddComponent(myComponent);
```

Alternatively you can use the *AddEntityComponent* convenience method. This will update the EntityList and add the component to the gameObject.

```
myEntity.AddEntityComponent<MyComponent>()
```

### *Texture Packer*

This project utilises the Texture Packer importer.[Documentation.](https://www.codeandweb.com/texturepacker/unity)

An example Texture Packer project is included in this template within the `Game/Creative/TexturePacker` folder.

Here you will find your .tps file in the `Data` folder. Your pre packed graphics in `PrepackedGraphics` and the final packed texture in `Resources`.

You can adjust 9 slicing of images by selecting the packed texture and using Unitys Sprite Editor.