//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nonatomic.Utomic {

	public enum ServiceStatus{
		Requesting,
		Complete,
		Error
	}

	public class Service : IService {

		private List<ServiceConfig> requests = new List<ServiceConfig>();

		public void Call(ServiceConfig config) {

			requests.Add(config);

			if(requests.Count == 1){
				ExecuteRequest(config);
				return;
			}
		}

		private void ExecuteRequest(ServiceConfig config){
			CoroutineRunner runner = this.EntityList().GetComponent<CoroutineRunner>();
			runner.Run(Request(config));
		}

		private IEnumerator Request(ServiceConfig config){

			this.Log("Service Request", config.endPoint, "orange", "yellow");
			ValidateConfig(config);

			WWW www = null;

			if(config.statusChange != null){
				config.status = ServiceStatus.Requesting;
				config.statusChange.Dispatch(config);
			}

			//check cache
			if(config.cache && IsCached(config.url)){

				//load from cache
				var cacheWWW = new WWW( "file://" + CacheName(config.url));

				// yield return cacheWWW;

				//progress
				while( !cacheWWW.isDone ){
					if(config.progress != null){
						config.progress.Dispatch(cacheWWW);
					}
					yield return null;
				}
				if(config.progress != null){
					config.progress.Dispatch(cacheWWW);
				}

				RequestComplete(config, cacheWWW, false);
			}

			var url = String.IsNullOrEmpty(config.endPoint) ? config.url : config.url + "/" + config.endPoint;

			switch(config.requestMethod){
				case RequestMethod.Get:
					www = new WWW(url);
				break;
				case RequestMethod.Post:
					www = new WWW(url, config.data);
				break;
			}

			// yield return www;

			//progress
			while( !www.isDone ){
				if(config.progress != null){
					config.progress.Dispatch(www);
				}
				yield return null;
			}
			if(config.progress != null){
				config.progress.Dispatch(www);
			}

			RequestComplete(config, www, true);
		}

		private void RequestComplete(ServiceConfig config, WWW www, bool errorCheck){
			
			if(errorCheck){
				CustomErrorCheck(config, www);
			}

			CustomSuccessCheck(config, www);

			//Remove request
			requests.PopAt(0);

			//Fire next request
			if(requests.Count > 0){
				ExecuteRequest(requests[0]);
			}
		}

		private void CustomErrorCheck(ServiceConfig config, WWW www){
			//custom error check
			bool customError = config.errorCheck != null && config.errorCheck.Check(www);

			if(customError || www.error != null){
				this.LogError("Service Error:" + config.endPoint + " - " + customError + ", " + www.error );
				if(config.error != null){
					config.error.Dispatch(www.error);

					if(config.statusChange != null){
						config.status = ServiceStatus.Error;
						config.statusChange.Dispatch(config);
					}
				}
			}
		}

		private void CustomSuccessCheck(ServiceConfig config, WWW www){
			//custom success check
			bool customSuccess = config.successCheck != null && config.successCheck.Check(www);
			if(customSuccess || www.error == null){
				
				if(config.complete != null){
					if(config.statusChange != null){
						config.status = ServiceStatus.Complete;
						config.statusChange.Dispatch(config);
					}

					config.complete.Dispatch(www);
				}
			}
		}

		private void ValidateConfig(ServiceConfig config) {

			if(config.url == null || String.IsNullOrEmpty(config.url)){
				this.LogWarning("ServiceConfig has no url");
			}
		}

		private string CacheName(string url){
			string justFilename = System.IO.Path.GetFileName(url);
			return Application.persistentDataPath + "/" + justFilename;
		}

		private bool IsCached(string url){
			return System.IO.File.Exists(CacheName(url));
		}
	}
}
