//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using System;
using UnityEngine;
using System.Collections;

namespace Nonatomic.Utomic {

	public enum RequestMethod {
			Post,
			Get
	};

	public class ServiceConfig {

		public RequestMethod requestMethod;
		public string name;
		public string url;
		public WWWForm data;
		public Signal error;
		public Signal complete;
		public Signal progress;
		public string endPoint;
		public Signal statusChange;
		public ServiceStatus status;
		public IServiceResultCheck errorCheck;
		public IServiceResultCheck successCheck;
		public bool cache = false;

	}
}
