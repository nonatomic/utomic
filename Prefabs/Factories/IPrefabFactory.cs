//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Nonatomic.Utomic {

	public interface IPrefabFactory  {

		/**
		* Makes and returns a prefab
		*/
		GameObject Make(string path);

		/**
		* Makes and returns a prefab whilst settings the prefabs parent
		*/
		GameObject Make(string path, GameObject parent);

		/**
		* Makes and returns a prefab as a button whilst settings the prefabs parent
		*/
		GameObject MakeButton(string path, GameObject parent, ButtonConfig buttonConfig);
		GameObject MakeButton(string path, GameObject parent, Signal click);
		T MakeButton<T>(string path, GameObject parent, ButtonConfig buttonConfig) where T:Component;
		T MakeButton<T>(string path, GameObject parent, Signal click) where T:Component;

		/**
		* Makes a prefab and adds component T
		* returning T
 		*/
		T Make<T>(string path) where T:Component;

		/**
		* Makes a prefab and adds component T
		* returning T component
 		*/
		T Make<T>(string path, GameObject parent) where T:Component;

		/**
		* Pop an instance off the pool or make a new prefab
		* returning T component
		*/
		T MakeFromPool<T>(string path, GameObject parent) where T:Component;

		/**
		* Pop an instance off the pool with a search term or make a new prefab
		* returning T component
		*/
		T MakeFromPool<T>(string path, GameObject parent, Predicate<T> search) where T:Component;

		/**
		* Pop an instance off the pool with a search term or make a new prefab
		* returning T component
		*/
		GameObject MakeFromPool(string path, GameObject parent, Predicate<GameObject> search);
	}
}
