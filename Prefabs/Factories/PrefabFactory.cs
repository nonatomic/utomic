//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Nonatomic.Utomic {

	public class PrefabFactory : IPrefabFactory  {

		/**
		* Makes and returns a prefab
		*/
		public GameObject Make(string path){
			var prefab = GameObject.Instantiate(Resources.Load(path)) as GameObject;
			prefab.name = path;
			this.EntityList().AddEntity(prefab);
			return prefab;
		}

		/**
		* Makes and returns a prefab whilst settings the prefabs parent
		*/
		public GameObject Make(string path, GameObject parent) {
			var prefab = GameObject.Instantiate(Resources.Load(path)) as GameObject;
			prefab.name = path;
			prefab.transform.SetParent(parent.transform, false);
			this.EntityList().AddEntity(prefab);
			return prefab;
		}

		/**
		* Makes and returns a prefab as a button whilst settings the prefabs parent
		*/
		public GameObject MakeButton(string path, GameObject parent, ButtonConfig buttonConfig) {

			var prefab = GameObject.Instantiate(Resources.Load(path)) as GameObject;
			prefab.name = path;
			prefab.AddEntityComponent<Button>();
			prefab.AddEntityComponent<ButtonMonitor>().SetConfig(buttonConfig);
			prefab.transform.SetParent(parent.transform, false);
			this.EntityList().AddEntity(prefab);

			//new to unity is the Raycast target. This much be set to true for buttons to work
			prefab.GetComponent<Image>().raycastTarget = true;

			return prefab;
		}

		public T MakeButton<T>(string path, GameObject parent, ButtonConfig buttonConfig) where T:Component {

			var prefab = MakeButton(path, parent, buttonConfig);
			prefab.name = path;
			var component = prefab.AddEntityComponent<T>();

			return component;
		}


		/**
		* Makes and returns a prefab as a button whilst settings the prefabs parent
		*/
		public GameObject MakeButton(string path, GameObject parent, Signal click) {

			var prefab = GameObject.Instantiate(Resources.Load(path)) as GameObject;
			prefab.name = path;
			prefab.AddEntityComponent<Button>();
			prefab.AddEntityComponent<ButtonMonitor>().SetConfig(new ButtonConfig(){click = click});
			prefab.transform.SetParent(parent.transform, false);
			this.EntityList().AddEntity(prefab);

			//new to unity is the Raycast target. This must be set to true for buttons to work
			var img = prefab.GetComponent<Image>();
			if(img == null)
				img = prefab.GetComponentInChildren<Image>();

			if(img != null)
				img.raycastTarget = true;

			return prefab;
		}

		public T MakeButton<T>(string path, GameObject parent, Signal click) where T:Component {

			var prefab = MakeButton(path, parent, click);
			var component = prefab.AddEntityComponent<T>();
			return component;
		}

		/**
		* Makes a prefab and adds component T
		* returning T
 		*/
		public T Make<T>(string path) where T:Component {
			var newPrefab = GameObject.Instantiate(Resources.Load(path)) as GameObject;
			newPrefab.name = path;

			if(newPrefab.GetComponent<T>() != null){
				this.LogWarning("Prefab " + path +" already contains component: " + typeof(T));
			}

			var component = newPrefab.AddEntityComponent<T>();
			this.EntityList().AddEntity(newPrefab);
			return component;
		}

		/**
		* Makes a prefab and adds component T
		* returning T component
 		*/
		public T Make<T>(string path, GameObject parent) where T:Component{
			var newPrefab = GameObject.Instantiate(Resources.Load(path)) as GameObject;
			newPrefab.name = path;
			newPrefab.transform.SetParent(parent.transform, false);

			if(newPrefab.GetComponent<T>() != null){
				this.LogWarning("Prefab " + path +" already contains component: " + typeof(T));
			}

			var component = newPrefab.AddEntityComponent<T>();
			this.EntityList().AddEntity(newPrefab);
			return component;
		}

		/**
		* Pop an instance off the pool or make a new prefab
		* returning T component
		*
		* Whether made or popped from the pool the entity requested will be
		* added to the EntityList
		*/
		public T MakeFromPool<T>(string path, GameObject parent) where T:Component{

			var pooledGo = this.Inject<IEntityPool>().Pop<T>();
			T component = default(T);

			if(pooledGo == null){
				component = Make<T>(path, parent);
			}
			else{
				component = pooledGo.GetComponent<T>();
			}

			return component;
		}

		/**
		* Pop an instance off the pool with a search term or make a new prefab
		* returning T component
		*
		* Whether made or popped from the pool the entity requested will be
		* added to the EntityList
		*/
		public T MakeFromPool<T>(string path, GameObject parent, Predicate<T> search) where T:Component{

			var pooledGo = this.Inject<IEntityPool>().Pop<T>(search);
			T component = default(T);

			if(pooledGo == null){
				component = Make<T>(path, parent);
			}
			else{
				component = pooledGo.GetComponent<T>();
			}

			return component;
		}

		/**
		* Pop an instance off the pool with a search term or make a new prefab
		* returning T component
		*
		* Whether made or popped from the pool the entity requested will be
		* added to the EntityList
		*/
		public GameObject MakeFromPool(string path, GameObject parent, Predicate<GameObject> search){

			var pooledGo = this.Inject<IEntityPool>().Pop(search);

			if(pooledGo == null){
				pooledGo = Make(path, parent);
			}

			return pooledGo;
		}
	}
}
