//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;

namespace Nonatomic.Utomic {

	public class LightingFactory : ILightingFactory {

		public Light Make(LightConfig config, GameObject parent) {

			var go = new GameObject(config.name);
			go.transform.SetParent(parent.transform);
			go.transform.localPosition = config.position;
			go.transform.eulerAngles = config.eulerAngles;

			var light = go.AddEntityComponent<Light>();
			light.type = config.type;
			light.color = config.color;
			light.shadows = config.shadows;


			this.EntityList().AddEntity(go);
			return light;
		}

		public T Make<T>(LightConfig config, GameObject parent) where T : Component {
			T component = Make(config, parent).gameObject.AddEntityComponent<T>();
			this.EntityList().AddComponent(component);
			return component;
		}
	}
}
