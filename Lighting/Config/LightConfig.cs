//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using UnityEngine;
using System;

namespace Nonatomic.Utomic {

	public class LightConfig {

		public string name = "Light";
		public Vector3 position = Vector3.zero;
		public Vector3 eulerAngles = Vector3.zero;

		public LightType type = LightType.Directional;//Directional | Spot | Point | Area
		public Color color = Color.white;
		public LightShadows shadows = LightShadows.Hard;//Hard | Soft | None
	}
}
